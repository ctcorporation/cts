﻿using CTS_Satellite.DTO;
using System;

using System.Linq;
using System.Windows.Forms;

namespace CTS_Satellite
{

    public partial class frmCSVDetails : Form
    {
        private TextBox edDataStart;
        private Label label7;
        private TextBox edHeaderStart;
        private Label label6;
        private CheckBox cbQuotations;
        private Button btnEvaluate;
        private Button btnSave;
        private TextBox edLastFieldNames;
        private TextBox edFirstFieldNAme;
        private Label label5;
        private Label label4;
        private CheckBox cbHasHeaders;
        private TextBox edFieldCount;
        private Label label3;
        private TextBox edDelimiter;
        private Label label2;
        private TextBox edCsvName;
        private Label label1;
        private Button btnClose;

        class CSVProfile : ICSVProfile
        {
            public string csvName { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
            public char csvDelimiter { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
            public int fieldCount { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
            public string fieldNameFirst { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
            public string fieldNameLast { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
            public bool hasHeaderRow { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        }

        public Guid profileID
        {
            get;

            set;
        }
        private Guid fileID
        {
            get; set;
        }

        public frmCSVDetails()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmCSVDetails_Load(object sender, EventArgs e)
        {
            fileID = Guid.Empty;
            GetCSVDetails(profileID);
        }

        private void GetCSVDetails(Guid profileID)
        {
            using (IUnitOfWork uow = new UnitOfWork(new Models.SatelliteContext(Globals.ConnString.ConnString)))
            {
                var fileDesc = uow.FileDescriptions.Find(x => x.PC_P == profileID).FirstOrDefault();
                if (fileDesc == null)
                {
                    return;
                }
                edCsvName.Text = fileDesc.PC_FileName;
                edDelimiter.Text = fileDesc.PC_Delimiter;
                edFieldCount.Text = fileDesc.PC_Fieldcount.ToString();
                edFirstFieldNAme.Text = fileDesc.PC_FirstFieldName;
                edLastFieldNames.Text = fileDesc.PC_LastFieldName;
                cbQuotations.Checked = (bool)fileDesc.PC_Quotations;
                cbHasHeaders.Checked = (bool)fileDesc.PC_HasHeader;
                edDataStart.Text = fileDesc.PC_DataStart.ToString();
                edHeaderStart.Text = fileDesc.PC_HeaderStart.ToString();
            }

        }

        private void btnSave_Click(object sender, EventArgs e)
        {

        }

        private void btnEvaluate_Click(object sender, EventArgs e)
        {

        }

        private string GetDelimiter(string fileName)
        {

            char[] pattern = new char[] { Convert.ToChar(";"), Convert.ToChar(","), Convert.ToChar("|"), Convert.ToChar("*"), Convert.ToChar(9) };
            int fCount = 0;
            char c = new char();
            for (int dCount = 0; dCount < pattern.Length; dCount++)
            {
                string[] fields = fileName.Split(pattern[dCount]);
                if (fields.Length > 0)
                {
                    if (fields.Length > fCount)
                    {
                        fCount = fields.Length;
                        c = pattern[dCount];
                    }

                }

            }

            return c.ToString();
        }

        private void InitializeComponent()
        {
            this.edDataStart = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.edHeaderStart = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbQuotations = new System.Windows.Forms.CheckBox();
            this.btnEvaluate = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.edLastFieldNames = new System.Windows.Forms.TextBox();
            this.edFirstFieldNAme = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cbHasHeaders = new System.Windows.Forms.CheckBox();
            this.edFieldCount = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.edDelimiter = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.edCsvName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // edDataStart
            // 
            this.edDataStart.Location = new System.Drawing.Point(463, 99);
            this.edDataStart.Name = "edDataStart";
            this.edDataStart.Size = new System.Drawing.Size(37, 20);
            this.edDataStart.TabIndex = 37;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(366, 102);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 13);
            this.label7.TabIndex = 36;
            this.label7.Text = "Data Starts at";
            // 
            // edHeaderStart
            // 
            this.edHeaderStart.Location = new System.Drawing.Point(463, 72);
            this.edHeaderStart.Name = "edHeaderStart";
            this.edHeaderStart.Size = new System.Drawing.Size(37, 20);
            this.edHeaderStart.TabIndex = 35;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(366, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 13);
            this.label6.TabIndex = 34;
            this.label6.Text = "Header Starts at";
            // 
            // cbQuotations
            // 
            this.cbQuotations.AutoSize = true;
            this.cbQuotations.Location = new System.Drawing.Point(414, 46);
            this.cbQuotations.Name = "cbQuotations";
            this.cbQuotations.Size = new System.Drawing.Size(135, 17);
            this.cbQuotations.TabIndex = 33;
            this.cbQuotations.Text = "Enclosed in Quotations";
            this.cbQuotations.UseVisualStyleBackColor = true;
            // 
            // btnEvaluate
            // 
            this.btnEvaluate.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
            this.btnEvaluate.Location = new System.Drawing.Point(658, 46);
            this.btnEvaluate.Name = "btnEvaluate";
            this.btnEvaluate.Size = new System.Drawing.Size(75, 23);
            this.btnEvaluate.TabIndex = 32;
            this.btnEvaluate.Text = "Evaluate";
            this.btnEvaluate.UseVisualStyleBackColor = true;
            this.btnEvaluate.Click += new System.EventHandler(this.btnEvaluate_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
            this.btnSave.Location = new System.Drawing.Point(657, 74);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 31;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // edLastFieldNames
            // 
            this.edLastFieldNames.Location = new System.Drawing.Point(134, 99);
            this.edLastFieldNames.Name = "edLastFieldNames";
            this.edLastFieldNames.Size = new System.Drawing.Size(205, 20);
            this.edLastFieldNames.TabIndex = 30;
            // 
            // edFirstFieldNAme
            // 
            this.edFirstFieldNAme.Location = new System.Drawing.Point(134, 72);
            this.edFirstFieldNAme.Name = "edFirstFieldNAme";
            this.edFirstFieldNAme.Size = new System.Drawing.Size(205, 20);
            this.edFirstFieldNAme.TabIndex = 29;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 102);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 13);
            this.label5.TabIndex = 28;
            this.label5.Text = "Name of Last Field";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 13);
            this.label4.TabIndex = 27;
            this.label4.Text = "Name of First Field";
            // 
            // cbHasHeaders
            // 
            this.cbHasHeaders.AutoSize = true;
            this.cbHasHeaders.Location = new System.Drawing.Point(300, 46);
            this.cbHasHeaders.Name = "cbHasHeaders";
            this.cbHasHeaders.Size = new System.Drawing.Size(108, 17);
            this.cbHasHeaders.TabIndex = 26;
            this.cbHasHeaders.Text = "Has Header Row";
            this.cbHasHeaders.UseVisualStyleBackColor = true;
            // 
            // edFieldCount
            // 
            this.edFieldCount.Location = new System.Drawing.Point(164, 43);
            this.edFieldCount.Name = "edFieldCount";
            this.edFieldCount.Size = new System.Drawing.Size(100, 20);
            this.edFieldCount.TabIndex = 25;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(143, 13);
            this.label3.TabIndex = 24;
            this.label3.Text = "Number of Fields in CSV File.";
            // 
            // edDelimiter
            // 
            this.edDelimiter.Location = new System.Drawing.Point(528, 11);
            this.edDelimiter.Name = "edDelimiter";
            this.edDelimiter.Size = new System.Drawing.Size(21, 20);
            this.edDelimiter.TabIndex = 23;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(451, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "CSV Delimiter";
            // 
            // edCsvName
            // 
            this.edCsvName.Location = new System.Drawing.Point(134, 11);
            this.edCsvName.Name = "edCsvName";
            this.edCsvName.Size = new System.Drawing.Size(283, 20);
            this.edCsvName.TabIndex = 21;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "CSV File Name default";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
            this.btnClose.Location = new System.Drawing.Point(657, 103);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 19;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // frmCSVDetails
            // 
            this.ClientSize = new System.Drawing.Size(744, 138);
            this.Controls.Add(this.edDataStart);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.edHeaderStart);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cbQuotations);
            this.Controls.Add(this.btnEvaluate);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.edLastFieldNames);
            this.Controls.Add(this.edFirstFieldNAme);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cbHasHeaders);
            this.Controls.Add(this.edFieldCount);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.edDelimiter);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.edCsvName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnClose);
            this.Name = "frmCSVDetails";
            this.ResumeLayout(false);
            this.PerformLayout();

        }
    }
}
