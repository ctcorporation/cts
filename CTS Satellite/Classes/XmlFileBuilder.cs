﻿using System;
using System.IO;
using System.Xml.Serialization;
using XMLLocker.Cargowise.XUS;

namespace CTS_Satellite
{
    public class XmlFileBuilder
    {
        #region members
        private string _outPutDir;
        private string _custCode;

        #endregion

        #region Properties
        public string OutPutDir
        {
            get { return _outPutDir; }
            set { _outPutDir = value; }
        }

        public string CustCode
        {
            get { return _custCode; }
            set { _custCode = value; }
        }

        public string ErrMsg { get; set; }
        #endregion

        #region Constructors
        public XmlFileBuilder(string custCode, string outDir)
        {
            OutPutDir = outDir;
            CustCode = custCode;
        }
        #endregion

        #region Methods

        public bool CreateFile(string refNo, UniversalInterchange interchange, XmlSerializerNamespaces ns)
        {
            int iFileCount = 0;
            var xmlOutFile = Path.Combine(_outPutDir, _custCode + "-" + refNo + ".xml");
            try
            {
                while (File.Exists(xmlOutFile))
                {
                    iFileCount++;
                    xmlOutFile = Path.Combine(_outPutDir, _custCode + "-" + refNo + "-" + iFileCount + ".xml");
                }
                using (Stream outputXML = File.Open(xmlOutFile, FileMode.Create))
                {
                    StringWriter writer = new StringWriter();
                    XmlSerializer xSer = new XmlSerializer(typeof(UniversalInterchange));
                    xSer.Serialize(outputXML, interchange, ns);

                    outputXML.Flush();
                    outputXML.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                string exMsg = "Error found creating file. " + xmlOutFile + ". Error " + ex.GetType().Name + Environment.NewLine
                                + "Error details: " + ex.Message + Environment.NewLine
                                + ex.InnerException;
                return false;
            }


        }
        #endregion

        #region Helpers

        #endregion
    }
}
