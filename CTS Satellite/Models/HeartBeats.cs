﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CTS_Satellite.Models
{
    [Table("HEARTBEAT")]
    public partial class HeartBeats

    {
        [Key]
        public Guid HB_ID { get; set; }



        [StringLength(50)]
        public string HB_NAME { get; set; }

        [StringLength(200)]
        public string HB_PATH { get; set; }

        [Required]
        [StringLength(1)]
        public string HB_ISMONITORED { get; set; }

        public int? HB_PID { get; set; }

        [StringLength(50)]
        public string HB_LASTOPERATION { get; set; }

        public DateTime? HB_LASTCHECKIN { get; set; }

        public DateTime? HB_OPENED { get; set; }

        [StringLength(100)]
        public string HB_LASTOPERATIONPARAMS { get; set; }

        #region Navigational
        [ForeignKey("CustomerLookup")]
        public Guid? HB_C { get; set; }

        public virtual Customer CustomerLookup
        {
            get;
            set;
        }

        #endregion

    }
}
