﻿using CTS_Satellite.Models;

namespace CTS_Satellite.Repository
{
    public class NodeCustRepository : GenericNodeRepository<Customer>, INodeCustRepository
    {
        public NodeCustRepository(NodeDataContext context)
            : base(context)
        {

        }

        public NodeDataContext NodeDataContext
        {
            get
            {
                return Context as NodeDataContext;
            }
        }
    }
}
