﻿using CTS_Satellite.Models;

namespace CTS_Satellite.Repository
{
    public interface ITransactionRepository : IGenericRepository<Transaction>
    {
        SatelliteContext SatelliteContext { get; }
    }
}