﻿using CTS_Satellite.Models;

namespace CTS_Satellite.Repository
{
    public class CargowiseContextRepository : GenericRepository<CargowiseContext>, ICargowiseContextRepository
    {
        public CargowiseContextRepository(SatelliteContext context)
            : base(context)
        {

        }

        public SatelliteContext SatelliteContext
        {
            get { return Context as SatelliteContext; }
        }
    }
}
