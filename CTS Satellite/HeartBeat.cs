﻿
using CTS_Satellite.DTO;
using CTS_Satellite.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;

namespace CTS_Satellite
{
    public static class HeartBeat
    {
        private static List<Assembly> GetListOfEntryAssemblyWithReferences()
        {
            List<Assembly> listOfAssemblies = new List<Assembly>();
            var mainAsm = Assembly.GetEntryAssembly();
            listOfAssemblies.Add(mainAsm);

            foreach (var refAsmName in mainAsm.GetReferencedAssemblies())
            {
                listOfAssemblies.Add(Assembly.Load(refAsmName));
            }
            return listOfAssemblies;
        }

        public static void RegisterHeartBeat(string custCode, string operation, ParameterInfo[] parameters)
        {
            try
            {

            }

            catch (Exception ex)
            {
                string strEx = ex.GetType().Name;


            }
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(Globals.CTCconnString())))
            {
                bool newHeartBeat = false;
                HeartBeats heartBeat = new HeartBeats();
                var cust = uow.NodeCust.Find(x => x.C_CODE == custCode).FirstOrDefault();
                if (cust == null)
                {
                    return;
                }
                heartBeat = uow.HBeats.Find(x => x.HB_C == cust.C_ID).FirstOrDefault();
                if (heartBeat == null)
                {
                    newHeartBeat = true;
                    heartBeat = new HeartBeats
                    {
                        HB_ID = Guid.NewGuid(),
                        HB_C = GetCustId(custCode),
                        HB_NAME = Assembly.GetExecutingAssembly().GetName().Name,
                        HB_PATH = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                System.Reflection.Assembly.GetExecutingAssembly().GetName().Name),
                        HB_ISMONITORED = "Y"

                    };

                }
                heartBeat.HB_LASTCHECKIN = DateTime.Now;
                heartBeat.HB_LASTOPERATION = operation;
                string pList = string.Empty;
                if (parameters != null)
                {
                    for (int i = 0; i < parameters.Length; i++)
                    {
                        pList += parameters[i].Name + ": " + parameters[i].ToString();
                    }
                }
                heartBeat.HB_LASTOPERATIONPARAMS = pList;
                if (operation == "Starting")
                {
                    heartBeat.HB_OPENED = DateTime.Now;
                    Process currentProcess = Process.GetCurrentProcess();
                    heartBeat.HB_PID = currentProcess.Id;
                }
                if (operation == "Stopping")
                {

                    heartBeat.HB_OPENED = null;
                    heartBeat.HB_LASTCHECKIN = null;
                    heartBeat.HB_PID = 0;

                }
                if (newHeartBeat)
                {
                    uow.HBeats.Add(heartBeat);
                }


                uow.NodeComplete();




            }
        }


        private static Guid? GetCustId(string custCode)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(Globals.CTCconnString())))
            {
                var cust = (uow.NodeCust.Find(c => c.C_CODE == custCode)).FirstOrDefault();
                if (cust != null)
                {
                    return cust.C_ID;
                }
            }
            return null;
        }
    }
}
