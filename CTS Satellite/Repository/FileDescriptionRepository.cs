﻿using CTS_Satellite.Models;

namespace CTS_Satellite.Repository
{
    public class FileDescriptionRepository : GenericRepository<FileDescription>, IFileDescriptionRepository
    {
        public FileDescriptionRepository(SatelliteContext context)
            : base(context)
        {
        }

        public SatelliteContext SatelliteContext
        {
            get { return Context as SatelliteContext; }
        }
    }
}
