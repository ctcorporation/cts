﻿namespace CTS_Satellite.Models
{
    public class OrderLines
    {

        public string PONumber { get; set; }

        public int LineNumber { get; set; }

        public string ProdCode { get; set; }

        public string Descr { get; set; }

        public string Channel { get; set; }

        public string PackCode { get; set; }

        public string SizeHeader { get; set; }

        public string SizeRatio { get; set; }

        public string PackSize { get; set; }

        public string Color { get; set; }

        public int Qty { get; set; }

        public string HSCode { get; set; }
    }
}
