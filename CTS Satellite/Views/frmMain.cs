﻿
using CTS_Satellite.Classes;
using CTS_Satellite.DTO;
using CTS_Satellite.Models;
using NodeResources;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Timers;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using XMLLocker.Cargowise;
using static CTS_Satellite.SatelliteErrors;

namespace CTS_Satellite
{

    public partial class frmMain : Form
    {
        public int totfilesRx;
        public int totfilesTx;
        public int cwRx;
        public int cwTx;
        public int filesRx;
        public int filesTx;
        public string filesPath;
        public System.Timers.Timer tmrMain;

        public frmMain()
        {
            InitializeComponent();
            tmrMain = new System.Timers.Timer();
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(OnProcessExit);
            this.Text = string.Format("CTC Satellite - EMOTrans {0}", Assembly.GetExecutingAssembly().GetName().Version.ToString());
        }

        private void OnProcessExit(object sender, EventArgs e)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, "Stopping", m.GetParameters());
        }


        private void tmrMain_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (btnTimer.Text == "&Stop")
            {
                tmrMain.Stop();
                GetFiles("Processing");
                GetFiles("Custom");
                tmrMain.Start();
            }
            else
            {
                tmrMain.Stop();
            }
        }

        private Customer getCustPath(String senderID)
        {
            using (DTO.IUnitOfWork uow = new UnitOfWork(new SatelliteContext(Globals.ConnString.ConnString)))
            {
                var cust = uow.Customers.Find(x => x.C_CODE == senderID).FirstOrDefault();
                if (cust != null)
                {
                    return cust;
                }
                return null;
            }

        }

        private void btnTimer_Click(object sender, EventArgs e)
        {
            if (((Button)sender).Text == "&Start")
            {
                ((Button)sender).Text = "&Stop";
                ((Button)sender).BackColor = System.Drawing.Color.LightCoral;
                tslMain.Text = "Timed Processes Started";
                GetFiles("Processing");

                GetFiles("Custom");
                //  PrintJobs("");
                tmrMain.Start();

            }
            else
            {
                ((Button)sender).Text = "&Start";
                ((Button)sender).BackColor = System.Drawing.Color.LightGreen;
                tslMain.Text = "Timed Processes Stopped";
                tmrMain.Stop();
            }
            this.tslSpacer.Padding = new Padding(this.Size.Width - tslMain.Width - 175, 0, 0, 0);
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void clearLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rtbLog.Text = "";

        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAbout AboutBox = new frmAbout();
            AboutBox.ShowDialog();
            LoadSettings();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {

            LoadSettings();
            HeartBeat.RegisterHeartBeat(Globals.glCustCode, "Starting", null);
            tslMode.Text = "Production";
            productionToolStripMenuItem.Checked = true;
            this.tslSpacer.Padding = new Padding(this.Size.Width - 175, 0, 0, 0);
        }

        private void LoadSettings()
        {
            var appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            var path = Path.Combine(appDataPath, System.Diagnostics.Process.GetCurrentProcess().ProcessName);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);


            }
            tmrMain.Interval = 60 * (1000 * 5);

            tmrMain.Elapsed += new ElapsedEventHandler(tmrMain_Elapsed);
            Globals.glAppConfig = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + ".XML";
            Globals.glAppConfig = Path.Combine(path, Globals.glAppConfig);
            frmSettings Settings = new frmSettings();
            if (!File.Exists(Globals.glAppConfig))
            {
                XDocument xmlConfig = new XDocument(
                    new XDeclaration("1.0", "UTF-8", "Yes"),
                    new XElement("Satellite",
                    new XElement("Customer"),
                    new XElement("Database"),
                    new XElement("CTCNode"),
                    new XElement("Communications")));
                xmlConfig.Save(Globals.glAppConfig);
                Settings.ShowDialog();
            }
            else
            {
                try
                {
                    bool loadConfig = false;
                    XmlDocument xmlConfig = new XmlDocument();
                    xmlConfig.Load(Globals.glAppConfig);
                    // Configuration Section DataBase
                    XmlNodeList nodeList = xmlConfig.SelectNodes("/Satellite/Database");
                    XmlNode nodeCat = nodeList[0].SelectSingleNode("Catalog");
                    if (nodeCat != null)
                    {
                        Globals.glDbInstance = nodeCat.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xmlServer = nodeList[0].SelectSingleNode("Servername");
                    if (xmlServer != null)
                    {
                        Globals.glDbServer = xmlServer.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xmlUser = nodeList[0].SelectSingleNode("Username");
                    if (xmlUser != null)
                    {
                        Globals.glDbUserName = xmlUser.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xmlPassword = nodeList[0].SelectSingleNode("Password");
                    if (xmlUser != null)
                    {
                        Globals.glDbPassword = xmlPassword.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    Globals.ConnString = new ConnectionManager(
                                        Globals.glDbServer,
                                        Globals.glDbInstance,
                                        Globals.glDbUserName,
                                        Globals.glDbPassword
                                        );


                    nodeList = null;
                    //Configuration Section Customer
                    nodeList = xmlConfig.SelectNodes("/Satellite/Customer");

                    XmlNode xCustCode = nodeList[0].SelectSingleNode("Code");
                    if (xCustCode != null)
                    {
                        Globals.glCustCode = xCustCode.InnerText;
                        var cust = NodeResources.GetCustomer(Globals.glCustCode);
                        if (cust != null)
                        {
                            Globals.glCustCode = cust.C_CODE;
                            Globals.gl_CustId = cust.C_ID;
                        }
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xPath = nodeList[0].SelectSingleNode("ProfilePath");
                    if (xPath != null)
                    {
                        Globals.glProfilePath = xPath.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xTestPath = nodeList[0].SelectSingleNode("TestPath");
                    if (xTestPath != null)
                    {
                        Globals.glTestLocation = xTestPath.InnerText;
                    }
                    XmlNode xArchivePath = nodeList[0].SelectSingleNode("ArchiveLocation");
                    if (xArchivePath != null)
                    {
                        Globals.glArcLocation = xArchivePath.InnerText;
                    }
                    XmlNode xCustomPath = nodeList[0].SelectSingleNode("CustomFilesPath");
                    if (xCustomPath != null)
                    {
                        Globals.glCustomFilesPath = xCustomPath.InnerText;
                    }

                    XmlNode xOutPath = nodeList[0].SelectSingleNode("OutputPath");
                    if (xOutPath != null)
                    {
                        Globals.glOutputDir = xOutPath.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xInPath = nodeList[0].SelectSingleNode("PickupPath");
                    if (xInPath != null)
                    {
                        Globals.glPickupPath = xInPath.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xLib = nodeList[0].SelectSingleNode("LibraryPath");
                    if (xLib != null)
                    {
                        Globals.glLibPath = xLib.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xFail = nodeList[0].SelectSingleNode("FailPath");
                    if (xFail != null)
                    {
                        Globals.glFailPath = xFail.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    var xExFact = nodeList[0].SelectSingleNode("MaxExFactoryDays");
                    if (xExFact != null)
                        Globals.glExFactDays = int.Parse(xExFact.InnerText);
                    else
                        loadConfig = true;                    

                    if (loadConfig)
                    {
                        throw new System.Exception("Mandatory Settings missing");
                    }

                    // Configuration Section Communications
                    nodeList = xmlConfig.SelectNodes("/Satellite/Communications");

                    XmlNode xmlAlertsTo = nodeList[0].SelectSingleNode("AlertsTo");
                    if (xmlAlertsTo != null)
                    {
                        Globals.glAlertsTo = xmlAlertsTo.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xSmtp = nodeList[0].SelectSingleNode("SMTPServer");
                    if (xSmtp != null)
                    {
                        SMTPServer.Server = xSmtp.InnerText;
                        SMTPServer.Port = xSmtp.Attributes[0].InnerXml;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xEmail = nodeList[0].SelectSingleNode("EmailAddress");
                    if (xEmail != null)
                    { SMTPServer.Email = xEmail.InnerText; }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xUserName = nodeList[0].SelectSingleNode("SMTPUsername");
                    if (xUserName != null)
                    {
                        SMTPServer.Username = xUserName.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xPassword = nodeList[0].SelectSingleNode("SMTPPassword");
                    if (xPassword != null)
                    {
                        SMTPServer.Password = xPassword.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    Globals.MailServerSettings = new MailServerSettings
                    {
                        Server = SMTPServer.Server,
                        UserName = SMTPServer.Username,
                        Password = SMTPServer.Password,
                        Port = Convert.ToInt16(SMTPServer.Port),
                        Email = SMTPServer.Email
                    };
                    //CTC Node Database Connection
                    nodeList = null;
                    nodeList = xmlConfig.SelectNodes("/Satellite/CTCNode");
                    nodeCat = null;
                    nodeCat = nodeList[0].SelectSingleNode("Catalog");
                    if (nodeCat != null)
                    {
                        Globals.glCTCDbInstance = nodeCat.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    xmlServer = null;
                    xmlServer = nodeList[0].SelectSingleNode("Servername");
                    if (xmlServer != null)
                    {
                        Globals.glCTCDbServer = xmlServer.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    xmlUser = null;
                    xmlUser = nodeList[0].SelectSingleNode("Username");
                    if (xmlUser != null)
                    {
                        Globals.glCTCDbUserName = xmlUser.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    xmlPassword = null;
                    xmlPassword = nodeList[0].SelectSingleNode("Password");
                    if (xmlUser != null)
                    {
                        Globals.glCTCDbPassword = xmlPassword.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                }

                catch (Exception ex)
                {
                    var exType = ex.GetType().Name;
                    MessageBox.Show("There was an error loading the Config files. Please check the settings", "Loading Settings", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    frmSettings FormSettings = new frmSettings();
                    FormSettings.ShowDialog();
                    DialogResult dr = FormSettings.DialogResult;
                    if (dr == DialogResult.OK)
                    {
                        LoadSettings();
                    }
                }
                var Cust = NodeResources.GetCustomer(Globals.glCustCode);
                try
                {
                    this.Text = "CTC Satellite - " + Cust.C_NAME;
                }
                catch (Exception)
                {
                    MessageBox.Show("There was an error loading the Config files. Please check the settings", "Loading Settings", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    frmSettings FormSettings = new frmSettings();
                    FormSettings.ShowDialog();
                    DialogResult dr = FormSettings.DialogResult;
                    if (dr == DialogResult.OK)
                    {
                        LoadSettings();
                    }
                }

                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "System Ready");
            }
        }



        private void profilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmProfiles Profiles = new frmProfiles();
            Profiles.ShowDialog();

        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSettings FormSettings = new frmSettings();
            FormSettings.ShowDialog();
            DialogResult dr = FormSettings.DialogResult;
            if (dr == DialogResult.OK)
            {
                LoadSettings();
            }
        }

        private void GetFiles(string folder)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            if (tslMode.Text == "Production")
            {
                filesPath = Path.Combine(Globals.glProfilePath, folder);
            }
            else
            {
                filesPath = Globals.glTestLocation;
            }
            DirectoryInfo diTodo = new DirectoryInfo(filesPath);
            int iCount = 0;
            var stopwatch = new Stopwatch();

            var list = diTodo.GetFiles();
            if (list.Length > 0)
            {
                stopwatch.Start();
                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Now Processing " + list.Length + " Files");
                foreach (var fileName in diTodo.GetFiles())
                {
                    iCount++;
                    ProcessResult thisResult = new ProcessResult();
                    thisResult = ProcessCustomFiles(fileName.FullName);
                    //return;
                    if (thisResult.Processed)
                    {
                        try
                        {
                            if (File.Exists(fileName.FullName))
                            {
                                System.GC.Collect();
                                System.GC.WaitForPendingFinalizers();
                                File.Delete(fileName.FullName);
                            }


                        }
                        catch (Exception ex)
                        {
                            NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Unable to Delete File. Error :" + ex.Message);
                        }

                    }

                }
                stopwatch.Stop();
                double t = stopwatch.Elapsed.TotalSeconds;
                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Processing Complete. " + iCount + " files processed in " + t + " seconds");
            }
            else
            {
                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Waiting for files.");
            }
        }

        private ProcessResult ProcessCustomFiles(String xmlFile)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            ProcessResult thisResult = new ProcessResult();
            thisResult.Processed = true;


            FileInfo processingFile = new FileInfo(xmlFile);
            filesRx++;
            NodeResources.AddLabel(lblFilesRx, filesRx.ToString());
            totfilesRx++;
            NodeResources.AddLabel(lblTotFilesRx, totfilesRx.ToString());
            TransReference trRef = new TransReference();
            NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "Processing file: " + Path.GetFileName(xmlFile));
            switch (processingFile.Extension.ToUpper())
            {
                case ".XML":
                    thisResult = ProcessCargowiseFiles(xmlFile);
                    break;
                case ".CSV":

                    ProcessCSV(new FileInfo(xmlFile));
                    break;
                case ".XLSX":
                    using (FileStream fs = new FileStream(xmlFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {

                        DataSet ds = new DataSet();
                        var dt = ExcelToTable.ToTable(xmlFile);
                        ds.Tables.Add(dt);
                        dt.Rows.RemoveAt(0);
                        dt.Rows.RemoveAt(0);
                        thisResult.Processed = true;
                        thisResult.FileName = xmlFile;
                        fs.Close();
                        fs.Dispose();
                    }
                    break;
                case ".IPI":
                    string strExtract = NodeResources.ExtractFile(xmlFile, "*.*", filesPath);
                    if (strExtract.Contains("Warning:"))
                    {
                        MessageBox.Show(strExtract);
                    }
                    else
                    {
                        Globals.ArchiveFile(Globals.glArcLocation, xmlFile);
                        string[] fileList = strExtract.Split(';');
                        foreach (string filename in fileList)
                        {
                            if (!string.IsNullOrEmpty(filename))
                            {
                                ProcessCustomFiles(Path.Combine(filesPath, filename));
                            }
                        }

                    }
                    break;
            }


            return thisResult;

        }



        private void ProcessCSV(FileInfo fi)
        {
            MethodBase m = MethodBase.GetCurrentMethod();
            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            DataTable dt = new DataTable();
            dt = NodeResources.ConvertCSVtoDataTable(fi.FullName);
            if (dt != null)
            {
                DataSet dsPCFS = new DataSet();
                try
                {
                    dsPCFS.Tables.Add(dt);
                    CityChicProcesses ccProcesses = new CityChicProcesses(dt, Globals.glCustCode, Globals.glOutputDir);
                    if (ccProcesses.ProcessFiles())
                    {
                        NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + ccProcesses.ErrMsg);
                    }

                    System.GC.Collect();
                    System.GC.WaitForPendingFinalizers();
                    File.Delete(fi.FullName);
                    TransReference tRef = new TransReference();
                }
                catch (Exception ex)
                {
                    string exMsg = ex.GetType().Name;
                    NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + exMsg + " Error Processing CSV File. ", Color.Red);
                    MailModule mail = new MailModule(Globals.MailServerSettings);
                    mail.SendMsg(fi.FullName, Globals.glAlertsTo, exMsg + " Error found processing CSV File", "Details" + ex.Message + Environment.NewLine + ex.InnerException + Environment.NewLine + ex.StackTrace);
                }
            }
            else
            {
                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "Error Processing CSV File. Not a valid File Type. Moving to Failed.");
                NodeResources.MoveFile(fi.FullName, Globals.glFailPath);
            }



        }



        private DataTable CreateStruct(string Enum, string EnumType)
        {
            using (DTO.IUnitOfWork uow = new UnitOfWork(new SatelliteContext(Globals.ConnString.ConnString)))
            {
                var custmaps = (from cm in uow.CustomMaps.GetAll()
                                join cw_en in uow.Enums.Find(x => x.CW_ENUMTYPE == EnumType && x.CW_ENUM == Enum) on cm.CF_CW equals cw_en.CW_ID
                                select cm).ToList();

            }
            DataTable result = new DataTable();

            return result;
        }


        private ProcessResult ProcessUDM(string xmlfile)
        {
            FileInfo processingFile = new FileInfo(xmlfile);
            ProcessResult thisResult = new ProcessResult();
            thisResult.Processed = true;
            CW.UniversalInterchange cwFile = new CW.UniversalInterchange();
            using (FileStream fStream = new FileStream(xmlfile, FileMode.Open))
            {
                XmlSerializer cwConvert = new XmlSerializer(typeof(CW.UniversalInterchange));
                cwFile = (CW.UniversalInterchange)cwConvert.Deserialize(fStream);
                fStream.Close();
            }
            if (cwFile.Body.BodyField == null)
            {
                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Cargowise File found but is not UDM. Attempting NDM: ");
                //thisResult = ProcessProductResponse(xmlfile);
                return thisResult;
            }
            totfilesRx++;
            NodeResources.AddLabel(lblTotFilesRx, totfilesRx.ToString());
            cwRx++;
            NodeResources.AddLabel(lblCwRx, cwRx.ToString());
            String senderID = String.Empty;
            String recipientID = String.Empty;
            String reasonCode = String.Empty;
            String eventCode = String.Empty;
            TransReference trRef = new TransReference();
            List<CW.DataSource> dscoll = new List<CW.DataSource>();
            CW.DataContext dc = new CW.DataContext();
            dc = cwFile.Body.BodyField.Shipment.DataContext;

            if (dc.DataSourceCollection != null)
            {
                try
                {
                    //dscoll = cwFile.Body.BodyField.Shipment.DataContext.DataSourceCollection.ToList();
                    dscoll = cwFile.Body.BodyField.Shipment.DataContext.DataSourceCollection.ToList();
                }
                catch (Exception)
                {

                    NodeResources.AddText(rtbLog, "XML File Missing DataSourceCollection tag");
                    ProcessingErrors procerror = new ProcessingErrors();
                    CTCErrorCode error = new CTCErrorCode();
                    error.Code = NodeError.e111;
                    error.Description = "DataSource Collection not found:";
                    error.Severity = "Fatal";
                    procerror.ErrorCode = error;
                    procerror.SenderId = senderID;
                    procerror.RecipientId = recipientID;
                    procerror.FileName = xmlfile;
                    procerror.ProcId = Guid.Empty;

                    //AddProcessingError(procerror);
                    thisResult.FolderLoc = Globals.glFailPath;
                    thisResult.Processed = false;
                    thisResult.FileName = xmlfile;
                    return thisResult;
                }

            }
            try
            {
                cwRx++;
                NodeResources.AddLabel(lblCwRx, cwRx.ToString());
                senderID = cwFile.Header.SenderID;
                recipientID = cwFile.Header.RecipientID;
                if (cwFile.Body.BodyField.Shipment.DataContext.EventType != null)
                {
                    eventCode = cwFile.Body.BodyField.Shipment.DataContext.EventType.Code;
                }
                else
                {
                    eventCode = string.Empty;
                }
                if (cwFile.Body.BodyField.Shipment.DataContext.ActionPurpose != null)
                {
                    reasonCode = cwFile.Body.BodyField.Shipment.DataContext.ActionPurpose.Code;
                }
                else
                {
                    reasonCode = string.Empty;
                }
                //reasonCode = cwFile.Body.BodyField.Shipment.DataContext.ActionPurpose.Code;
                Profile custProfileRecord = GetCustomerProfile(senderID, recipientID, reasonCode, eventCode);


                if (custProfileRecord.P_MSGTYPE.Trim() == "Conversion")
                {
                    // Rename the Cargowise XML File to the new Global XML
                    thisResult = ConvertCWFile(custProfileRecord, xmlfile);

                }
                return thisResult;
            }
            catch (Exception ex)
            {
                string strEx = ex.GetType().Name;
            }
            return thisResult;

        }

        private ProcessResult ConvertCWFile(Profile cust, String file)
        {
            ProcessResult result = new ProcessResult();
            NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "Beginning Cargowise Conversion: " + file);


            if (!String.IsNullOrEmpty(cust.P_METHOD))
            {
                string custMethod;
                custMethod = cust.P_METHOD;
                string custParams = cust.P_PARAMLIST;
                Type custType = this.GetType();
                MethodInfo custMethodToRun = custType.GetMethod(custMethod);
                try
                {
                    var varResult = custMethodToRun.Invoke(this, new Object[] { file, cust });
                    if (varResult != null)
                    {
                        //AddTransaction((Guid)dr["C_ID"], (Guid)dr["P_ID"], xmlFile, msgDir, true, (TransReference)varResult, archiveFile);

                        result.FileName = file;
                        result.Processed = true;
                    }
                }
                catch (Exception ex)
                {
                    string strEx = ex.GetType().Name;
                    strEx += " " + ex.Message;
                }


            }
            return result;


        }

        private ProcessResult ProcessCargowiseFiles(string xmlfile)
        {
            FileInfo processingFile = new FileInfo(xmlfile);
            NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " File Found " + Path.GetFileName(xmlfile) + ". Checking for Cargowise File.");
            var ns = CWFunctions.GetNameSpace(xmlfile);
            var appCode = CWFunctions.GetXMLType(xmlfile);
            String senderID = String.Empty;
            String recipientID = String.Empty;
            String reasonCode = String.Empty;
            ProcessResult canDelete = new ProcessResult();
            switch (appCode)
            {

                case "UDM":
                    NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " File is Cargowise (UDM) XML File :" + xmlfile + ". " + "");
                    if (CWFunctions.GetXMLType(xmlfile) == "UniversalEvent")
                    {
                        //canDelete = ProcessUDM(xmlfile);
                    }
                    else
                    {
                        canDelete = ProcessUDM(xmlfile);
                    }
                    break;
                case "NDM":
                    NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " File is Cargowise (NDM) XML File :" + xmlfile + ". " + "");
                    //  canDelete = ProcessProductResponse(xmlfile);
                    break;
                default:
                    NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " File is not a valid Cargowise XML File :" + xmlfile + ". Checking Client Specific formats" + "");
                    canDelete = ProcessCustomXML(xmlfile);
                    break;
            }

            if (canDelete.Processed && !string.IsNullOrEmpty(canDelete.FileName))
            {
                System.GC.Collect();
                System.GC.WaitForPendingFinalizers();
                File.Delete(canDelete.FileName);
            }
            else
            {
                if (canDelete.FileName != null)
                {
                    string f = NodeResources.MoveFile(xmlfile, canDelete.FolderLoc);
                    if (f.StartsWith("Warning"))
                    {
                        NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + f, System.Drawing.Color.Red);
                    }
                    else
                    { xmlfile = f; }
                }
            }
            return canDelete;
        }


        private ProcessResult ProcessCustomXML(string XMLFile)
        {
            ProcessResult result = new ProcessResult();

            return result;

        }

        private void customMappingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmEnums Enums = new frmEnums();
            Enums.Show();

        }

        private void bbClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }


        private Profile GetCustomerProfile(string senderID, string recipientID, string reasonCode, string eventCode)
        {
            using (DTO.IUnitOfWork uow = new UnitOfWork(new SatelliteContext(Globals.ConnString.ConnString)))
            {
                var profile = uow.Profiles.Find(x => x.P_SENDERID == senderID &&
                                                x.P_RECIPIENTID == recipientID &&
                                                x.P_ACTIVE == "Y");
                if (profile == null)
                {

                    Profile newprofile = new Profile
                    {
                        P_ID = Guid.NewGuid(),
                        P_C = Globals.gl_CustId,
                        P_REASONCODE = reasonCode.ToUpper(),
                        P_EVENTCODE = eventCode.ToUpper(),
                        P_DELIVERY = "R",
                        P_MSGTYPE = "Original",
                        P_SENDERID = senderID,
                        P_RECIPIENTID = recipientID,
                        P_DIRECTION = "R",
                        P_DESCRIPTION = "No Profile Found. Default Created"

                    };
                    uow.Profiles.Add(newprofile);
                    uow.Complete();
                    NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Create New Profile Sender ID: " + senderID + " Recipient ID: " + recipientID + " Purpose Code: " + reasonCode + " Event Code:" + eventCode);

                }
                Profile result = new Profile();
                foreach (var cp in profile)
                {
                    if (cp.P_REASONCODE == reasonCode)
                    {
                        if (cp.P_EVENTCODE == eventCode)
                        {
                            result = cp;
                            break;
                        }
                        else if (cp.P_EVENTCODE == "*")
                        {
                            result = cp;
                            break;
                        }
                    }
                    else if (cp.P_REASONCODE == "*")
                    {
                        if (cp.P_EVENTCODE == eventCode)
                        {
                            result = cp;
                            break;
                        }
                        else if (cp.P_EVENTCODE == "*")
                        {
                            result = cp;
                            break;
                        }
                    }

                }
                return result;
            }
        }


        private void frmMain_Resize(object sender, EventArgs e)
        {
            this.tslSpacer.Padding = new Padding(this.Size.Width - tslMain.Width - 175, 0, 0, 0);
        }

        private void productionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (productionToolStripMenuItem.Checked)
            {
                testingToolStripMenuItem.Checked = false;
                tslMode.Text = "Production";
            }
            else
            {
                testingToolStripMenuItem.Checked = true;
                tslMode.Text = "Testing";
            }
        }

        private void testingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (testingToolStripMenuItem.Checked)
            {
                productionToolStripMenuItem.Checked = false;
                tslMode.Text = "Testing";
            }
            else
            {
                productionToolStripMenuItem.Checked = true;
                tslMode.Text = "Production";
            }
        }

        private void fieldMappingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmFieldMapping fieldMapping = new frmFieldMapping();
            fieldMapping.Show();

        }

        private void organisationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmOrganisation orgs = new frmOrganisation();
            orgs.Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }


    }
}

