﻿using CTS_Satellite.DTO;
using CTS_Satellite.Models;
using System;

using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace CTS_Satellite
{
    public partial class frmNewCustomer : Form
    {

        public frmNewCustomer()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            using (IUnitOfWork uow = new UnitOfWork(new SatelliteContext(Globals.ConnString.ConnString)))
            {
                bool newcust = false;
                var cust = uow.Customers.Find(x => x.C_CODE == edCustCode.Text && x.C_NAME == edCustomerName.Text).FirstOrDefault();
                if (cust == null)
                {
                    cust = new Customer
                    {

                        C_CODE = edCustCode.Text,
                        C_NAME = edCustomerName.Text,
                        C_SHORTNAME = edShortName.Text


                    };
                    Globals.gl_CustId = Guid.NewGuid();
                    cust.C_ID = Globals.gl_CustId;
                    newcust = true;
                }
                cust.C_IS_ACTIVE = cbActive.Checked ? "Y" : "N";
                cust.C_ON_HOLD = cbHold.Checked ? "Y" : "N";
                cust.C_FTP_CLIENT = cbFtpClient.Checked ? "Y" : "N";
                cust.C_PATH = edRootPath.Text;
                cust.C_TRIAL = cbTrial.Checked ? "Y" : "N";
                if (cbTrial.Checked)
                {

                    cust.C_TRIALSTART = dtFrom.Value;
                    cust.C_TRIALEND = dtTo.Value;
                }
                if (newcust)
                {
                    uow.Customers.Add(cust);
                }
                if (uow.Complete() > 0)
                {
                    if (newcust)
                    {
                        MessageBox.Show("Customer Added", "Customer Added", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Customer already exists. Customer updated", "Customer Updated", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Globals.CreateRootPath(edRootPath.Text);
                    }
                    try
                    {
                        if (Directory.Exists(Path.Combine(edRootPath.Text, edCustCode.Text)))
                        {
                            DialogResult dr = MessageBox.Show("This Directory already Exists. Are you sure you wish to use ?", "Root Folder Exists", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (dr == DialogResult.Yes)
                            {
                                Globals.CreateRootPath(edRootPath.Text);
                            }
                        }
                        else
                        {
                            Globals.CreateRootPath(edRootPath.Text);
                        }
                        btnSave.Text = "&Update";

                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            btnSave.Text = "&Save";

        }

        private void GetCustomer(string custCode)
        {
            using (IUnitOfWork uow = new UnitOfWork(new SatelliteContext(Globals.connString())))
            {
                var cust = (uow.Customers.Find(x => x.C_CODE == custCode)).FirstOrDefault();
                if (cust != null)
                {
                    edCustCode.Text = cust.C_CODE;
                    edCustomerName.Text = cust.C_NAME;
                    edShortName.Text = cust.C_SHORTNAME;
                    edRootPath.Text = cust.C_PATH;
                    cbActive.Checked = cust.C_IS_ACTIVE == "Y" ? true : false;
                    cbHold.Checked = cust.C_ON_HOLD == "Y" ? true : false;
                    cbFtpClient.Checked = cust.C_FTP_CLIENT == "Y" ? true : false;
                    btnSave.Text = "&Update";
                    Globals.gl_CustId = cust.C_ID;
                }
                else
                {
                    btnSave.Text = "&Save";
                    edRootPath.Text = Globals.glProfilePath;
                    edCustCode.Text = Globals.glCustCode;

                }

            }

        }

        private void frmNewCustomer_Load(object sender, EventArgs e)
        {
            GetCustomer(Globals.glCustCode);
        }
    }
}
