﻿using CTS_Satellite.Models;

namespace CTS_Satellite.Repository
{
    public interface IOrganisationRepository : IGenericRepository<Organisation>
    {
        SatelliteContext SatelliteContext { get; }
    }
}