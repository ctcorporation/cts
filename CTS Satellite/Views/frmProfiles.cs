﻿using CTS_Satellite.DTO;
using CTS_Satellite.Models;
using System;
using System.Data;

using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;

namespace CTS_Satellite
{
    public partial class frmProfiles : Form
    {
        public Guid r_id;
        public Guid gP_id;

        public frmProfiles()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DeliveryChecked(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            String rbt = rb.Tag.ToString();
            switch (rbt)
            {
                case "pickup":
                    lblReceivePassword.Visible = false;
                    edReceiveUsername.Visible = false;
                    edReceivePassword.Visible = false;
                    lblReceiveUsername.Visible = false;
                    lblServerAddress.Visible = false;
                    edServerAddress.Visible = false;
                    lblFTPReceivePort.Visible = false;
                    edFTPReceivePort.Visible = false;
                    edFTPReceivePath.Visible = false;
                    lblFTPReceivePath.Visible = false;


                    break;
                case "eadapter":
                    lblReceivePassword.Visible = true;
                    edReceiveUsername.Visible = true;
                    edReceivePassword.Visible = true;
                    lblReceiveUsername.Visible = true;
                    lblServerAddress.Visible = true;
                    lblServerAddress.Text = "eAdapter URL";
                    edServerAddress.Visible = true;
                    lblFTPReceivePort.Visible = false;
                    edFTPReceivePort.Visible = false;
                    edFTPReceivePath.Visible = false;
                    lblFTPReceivePath.Visible = false;
                    break;
                case "ftp":
                    lblReceivePassword.Visible = true;
                    edReceiveUsername.Visible = true;
                    edReceivePassword.Visible = true;
                    lblReceiveUsername.Visible = true;
                    lblServerAddress.Visible = true;
                    lblServerAddress.Text = "Server Address";
                    edServerAddress.Visible = true;
                    lblFTPReceivePort.Visible = true;
                    edFTPReceivePort.Visible = true;
                    edFTPReceivePath.Visible = true;
                    lblFTPReceivePath.Visible = true;

                    break;
                case "email":
                    lblReceivePassword.Visible = false;
                    edReceiveUsername.Visible = false;
                    edReceivePassword.Visible = false;
                    lblReceiveUsername.Visible = false;
                    lblServerAddress.Visible = true;
                    edServerAddress.Visible = true;
                    lblServerAddress.Text = "Email Address";
                    lblFTPReceivePort.Visible = false;
                    edFTPReceivePort.Visible = false;
                    edFTPReceivePath.Visible = false;
                    lblFTPReceivePath.Visible = false;
                    break;
                default:
                    lblReceivePassword.Visible = true;
                    edReceiveUsername.Visible = true;
                    edReceivePassword.Visible = true;
                    lblReceiveUsername.Visible = true;
                    lblServerAddress.Visible = true;
                    lblServerAddress.Text = "Server Address";
                    edServerAddress.Visible = true;
                    lblFTPReceivePort.Visible = true;
                    edFTPReceivePort.Visible = true;
                    edFTPReceivePath.Visible = false;
                    lblFTPReceivePath.Visible = false;
                    break;
            }
        }


        private void frmProfiles_Load(object sender, EventArgs e)
        {

            r_id = Globals.gl_CustId;
            if (r_id == Guid.Empty)
            {
                frmNewCustomer NewCustomer = new frmNewCustomer();
                if (NewCustomer.ShowDialog(this) == DialogResult.OK)
                {
                    frmProfiles_Load(sender, e);
                }
            }
            else
            {
                FillGrid(r_id);
                ddlDTSType.DisplayMember = "Text";
                ddlDTSType.ValueMember = "Value";

                var items = new[] {
            new { Text = "Replace", Value = "R" },
            new { Text = "Update", Value = "U" },
            new { Text = "Delete", Value = "D" },
            new { Text = "Modify", Value = "M" }};
                ddlDTSType.DataSource = items;
            }

            // From PCFS Satellite
            //using (IUnitOfWork uow = new UnitOfWork(new SatelliteModel(Globals.glConnString)))
            //{
            //    var mappings = uow.MapOperations.GetDistinctMaps();
            //    mappings.Insert(0, "(none selected");
            //    if (mappings != null)
            //    {
            //        cmbMapping.DataSource = mappings;

            //    }
            //}


        }

        private void FillGrid(Guid C_id)
        {
            if (C_id != Guid.Empty)
            {
                using (IUnitOfWork uow = new UnitOfWork(new SatelliteContext(Globals.connString())))
                {
                    var profiles = (uow.Profiles.Find(x => x.P_C == r_id)).ToList();
                    if (profiles != null)
                    {
                        dgProfile.DataSource = profiles;
                    }
                }

            }
        }

        private void rbCustomer_CheckedChanged(object sender, EventArgs e)
        {
            if (rbSend.Checked)
            {
                gbReceiveOptions.Enabled = false;
                gbSendOptions.Enabled = true;
            }
            else
            {
                gbReceiveOptions.Enabled = true;
                gbSendOptions.Enabled = false;
            }
        }

        private void rbVendor_CheckedChanged(object sender, EventArgs e)
        {
            if (rbSend.Checked)
            {
                gbReceiveOptions.Enabled = false;
                gbSendOptions.Enabled = true;
            }
            else
            {
                gbReceiveOptions.Enabled = true;
                gbSendOptions.Enabled = false;
            }
        }

        private void ReadOnlycontrols(Boolean toggle)
        {
            if (!toggle)
            {

                cbDTS.Enabled = true;

            }
            else
            {

                cbDTS.Enabled = false;
            }
        }

        void SetupReceive()
        {
            String strReceive = String.Empty;
            foreach (RadioButton rb in gbReceiveMethod.Controls)
            {
                if (rb.Checked)
                {
                    switch (rb.Name)
                    {


                        case "rbPickup":
                            lblServerAddress.Visible = false;
                            edServerAddress.Visible = false;
                            lblFTPReceivePort.Visible = false;
                            edFTPReceivePort.Visible = false;
                            lblFTPReceivePath.Visible = false;
                            edFTPReceivePath.Visible = false;
                            lblReceiveUsername.Visible = false;
                            edReceiveUsername.Visible = false;
                            lblReceivePassword.Visible = false;
                            edReceivePassword.Visible = false;
                            lblReceiveEmailAddress.Visible = false;
                            edReceiveEmailAddress.Visible = false;
                            btnReceiveFolder.Visible = false;
                            edReceiveCustomerName.Visible = true;
                            lblReceiveCustomerName.Visible = true;
                            cbSSL.Visible = false;

                            break;
                        case "rbReceiveFTP":
                            lblServerAddress.Visible = true;
                            edServerAddress.Visible = true;
                            lblFTPReceivePort.Visible = true;
                            edFTPReceivePort.Visible = true;
                            lblFTPReceivePath.Visible = true;
                            edFTPReceivePath.Visible = true;
                            lblReceiveUsername.Visible = true;
                            edReceiveUsername.Visible = true;
                            lblReceivePassword.Visible = true;
                            edReceivePassword.Visible = true;
                            lblReceiveEmailAddress.Visible = false;
                            edReceiveEmailAddress.Visible = false;
                            btnReceiveFolder.Visible = false;
                            edReceiveCustomerName.Visible = true;
                            lblReceiveCustomerName.Visible = true;
                            cbSSL.Visible = false;
                            break;

                        case "rbEmail":
                            lblServerAddress.Visible = true;
                            edServerAddress.Visible = true;
                            lblFTPReceivePort.Visible = true;
                            edFTPReceivePort.Visible = true;
                            lblFTPReceivePath.Visible = false;
                            edFTPReceivePath.Visible = false;
                            lblReceiveUsername.Visible = true;
                            edReceiveUsername.Visible = true;
                            lblReceivePassword.Visible = true;
                            edReceivePassword.Visible = true;
                            btnReceiveFolder.Visible = false;
                            lblReceiveEmailAddress.Visible = true;
                            edReceiveEmailAddress.Visible = true;
                            lblReceiveCustomerName.Visible = true;
                            edReceiveCustomerName.Visible = true;

                            cbSSL.Visible = true;
                            break;
                        case "rbReceivePickup":
                            lblServerAddress.Visible = false;
                            edServerAddress.Visible = false;
                            lblFTPReceivePort.Visible = false;
                            edFTPReceivePort.Visible = false;
                            lblFTPReceivePath.Visible = true;
                            edFTPReceivePath.Visible = true;
                            btnReceiveFolder.Visible = true;
                            lblReceiveUsername.Visible = false;
                            edReceiveUsername.Visible = false;
                            lblReceivePassword.Visible = false;
                            edReceivePassword.Visible = false;
                            lblReceiveEmailAddress.Visible = false;
                            edReceiveEmailAddress.Visible = false;
                            edReceiveCustomerName.Visible = true;
                            lblReceiveCustomerName.Visible = true;
                            cbSSL.Visible = false;
                            break;

                    }
                }
            }
        }

        void SetupSend()
        {
            String strSend = String.Empty;

            foreach (RadioButton rb in gbSendMethod.Controls)
            {

                if (rb.Checked == true)
                {

                    switch (rb.Name)
                    {
                        case "rbPickupSend":
                            lblSendServer.Visible = true;
                            lblSendServer.Text = "Pickup Folder";
                            edSendCTCUrl.Visible = true;
                            lblSendUsername.Visible = false;
                            edSendUsername.Visible = false;
                            lblSendPassword.Visible = false;
                            edSendPassword.Visible = false;
                            lblSendPath.Visible = false;
                            edSendFolder.Visible = false;
                            btnPickupFolder.Visible = true;
                            lblTestResults.Visible = false;
                            edSendeAdapterResults.Visible = false;
                            btnTestSendeAdapter.Visible = false;
                            lblSendEmailAddress.Visible = false;
                            edSendEmailAddress.Visible = false;
                            edSendFtpPort.Visible = false;
                            lblSendFTPPort.Visible = false;
                            lblSendSubject.Visible = false;
                            edSendSubject.Visible = false;
                            break;
                        case "rbCTCSend":
                            lblSendServer.Visible = true;
                            lblSendServer.Text = "Server Address";
                            edSendCTCUrl.Visible = true;
                            lblSendUsername.Visible = true;
                            edSendUsername.Visible = true;
                            lblSendPassword.Visible = true;
                            edSendPassword.Visible = true;
                            lblSendPath.Visible = false;
                            edSendFolder.Visible = false;
                            lblTestResults.Visible = true;
                            btnPickupFolder.Visible = false;
                            edSendeAdapterResults.Visible = true;
                            btnTestSendeAdapter.Visible = true;
                            lblSendEmailAddress.Visible = false;
                            edSendEmailAddress.Visible = false;
                            lblSendSubject.Visible = false;
                            edSendSubject.Visible = false;
                            edSendFtpPort.Visible = false;
                            lblSendFTPPort.Visible = false;
                            break;
                        case "rbSoapSend":
                            lblSendServer.Visible = true;
                            edSendCTCUrl.Visible = true;
                            lblSendServer.Text = "Server Address";
                            lblSendUsername.Visible = true;
                            edSendUsername.Visible = true;
                            lblSendPassword.Visible = true;
                            edSendPassword.Visible = true;
                            lblSendPath.Visible = false;
                            edSendFolder.Visible = false;
                            lblTestResults.Visible = true;
                            btnPickupFolder.Visible = false;
                            edSendeAdapterResults.Visible = true;
                            btnTestSendeAdapter.Visible = true;
                            lblSendEmailAddress.Visible = false;
                            edSendEmailAddress.Visible = false;
                            lblSendSubject.Visible = true;
                            lblSendSubject.Text = "Endpoint";
                            edSendSubject.Visible = false;
                            edSendFtpPort.Visible = false;
                            lblSendFTPPort.Visible = false;
                            break;
                        case "rbFTPSend":
                            lblSendServer.Visible = true;
                            edSendCTCUrl.Visible = true;
                            lblSendServer.Text = "Server Address";
                            lblSendUsername.Visible = true;
                            edSendUsername.Visible = true;
                            lblSendPassword.Visible = true;
                            edSendPassword.Visible = true;
                            lblSendPath.Visible = true;
                            edSendFolder.Visible = true;
                            btnPickupFolder.Visible = false;
                            lblTestResults.Visible = true;
                            edSendeAdapterResults.Visible = true;
                            btnTestSendeAdapter.Visible = true;
                            lblSendEmailAddress.Visible = false;
                            edSendEmailAddress.Visible = false;
                            lblSendSubject.Visible = false;
                            edSendSubject.Visible = false;
                            lblSendFTPPort.Visible = true;
                            break;
                        case "rbEmailSend":
                            lblSendServer.Visible = false;
                            edSendCTCUrl.Visible = false;
                            lblSendServer.Text = "Server Address";
                            lblSendUsername.Visible = false;
                            edSendUsername.Visible = false;
                            lblSendPassword.Visible = false;
                            edSendPassword.Visible = false;
                            lblSendPath.Visible = false;
                            edSendFolder.Visible = false;
                            btnPickupFolder.Visible = false;
                            lblTestResults.Visible = false;
                            edSendeAdapterResults.Visible = false;
                            btnTestSendeAdapter.Visible = false;
                            lblSendEmailAddress.Visible = true;
                            edSendEmailAddress.Visible = true;
                            lblSendSubject.Visible = true;
                            edSendSubject.Visible = true;
                            lblSendSubject.Text = "Subject";
                            edSendFtpPort.Visible = false;
                            lblSendFTPPort.Visible = false;
                            break;
                        case "rbFTPSendDirect":
                            lblSendServer.Visible = true;
                            edSendCTCUrl.Visible = true;
                            lblSendServer.Text = "Server Address";
                            lblSendUsername.Visible = true;
                            edSendUsername.Visible = true;
                            lblSendPassword.Visible = true;
                            edSendPassword.Visible = true;
                            lblSendPath.Visible = true;
                            edSendFolder.Visible = true;
                            btnPickupFolder.Visible = false;
                            lblTestResults.Visible = true;
                            edSendeAdapterResults.Visible = true;
                            btnTestSendeAdapter.Visible = true;
                            lblSendEmailAddress.Visible = false;
                            edSendEmailAddress.Visible = false;
                            lblSendSubject.Visible = false;
                            edSendSubject.Visible = false;
                            edSendFtpPort.Visible = true;
                            lblSendFTPPort.Visible = true;
                            break;
                    }
                }

            }
        }

        private void FillDTSGrid(Guid pid)
        {
            using (IUnitOfWork uow = new UnitOfWork(new SatelliteContext(Globals.connString())))
            {
                var dts = (uow.DTServices.Find(x => x.D_P == pid)).ToList();
                if (dts != null)
                {
                    dgDts.DataSource = dts;
                }
            }

        }

        void FillCWContext()
        {
            using (IUnitOfWork uow = new UnitOfWork(new SatelliteContext(Globals.connString())))
            {
                var cwContext = uow.CWContext.GetAll().ToList();
                if (cwContext.Count > 0)
                {
                    var ddlSource = (from x in cwContext
                                     orderby x.CC_Context
                                     select new DataItem { ItemValue = x.CC_ID.ToString(), Text = x.CC_Context }).ToList();
                    ddlCWContext.ValueMember = "ItemValue";
                    ddlCWContext.DisplayMember = "Text";
                    ddlCWContext.DataSource = ddlSource;
                }
            }

        }

        private void ClearControls(bool allControls)
        {
            if (allControls)
            {

            }
            tvwXSD.ResetText();
            edReceiveCustomerName.Text = "";
            edDescription.Text = "";
            edFTPReceivePort.Text = "";
            edReasonCode.Text = "";
            edRecipientID.Text = "";
            edReceivePassword.Text = "";
            edReceiveUsername.Text = "";
            edServerAddress.Text = "";
            btnAdd.Text = "&Add >>";
            edDTSCurrentValue.Text = "";
            rbReceive.Checked = false;
            rbSend.Checked = false;
            edReasonCode.Text = "";
            edEventCode.Text = "";
            cmbMsgType.SelectedIndex = -1;
            cbChargeable.Checked = false;
            cbGroupCharges.Checked = false;
            edServerAddress.Text = "";
            edFTPReceivePort.Text = "";
            edFTPReceivePath.Text = "";
            btnXsdPath.Text = "";
            edReceiveSubject.Text = "";
            edReceiveEmailAddress.Text = "";
            edReceiveSender.Text = "";
            edReceiveSubject.Text = "";
            edXSDPath.Text = "";
            edCustomMethod.Text = "";
            lbParameters.Items.Clear();
            edLibraryName.Text = "";
            edParameter.Text = "";
        }

        public void ConvertXMLNodeToTreeNode(XmlNode xmlNode, TreeNodeCollection treeNodes)
        {
            TreeNode newTreeNode = treeNodes.Add(xmlNode.Name);
            switch (xmlNode.NodeType)
            {
                case XmlNodeType.ProcessingInstruction:
                case XmlNodeType.XmlDeclaration:
                    newTreeNode.Text = "<?" + xmlNode.Name + " " +
                      xmlNode.Value + "?>";
                    break;
                case XmlNodeType.Element:
                    newTreeNode.Text = "<" + xmlNode.Name + ">";
                    break;
                case XmlNodeType.Attribute:
                    newTreeNode.Text = "atrribute: " + xmlNode.Name;
                    break;
                case XmlNodeType.Text:
                case XmlNodeType.CDATA:
                    newTreeNode.Text = xmlNode.Value;
                    break;
                case XmlNodeType.Comment:
                    newTreeNode.Text = "<!--" + xmlNode.Value + "-->";
                    break;

            }
            if (xmlNode.Attributes != null)
            {
                foreach (XmlAttribute attrib in xmlNode.Attributes)
                {
                    ConvertXMLNodeToTreeNode(attrib, newTreeNode.Nodes);
                }
            }
            if (xmlNode.HasChildNodes)
            {
                foreach (XmlNode childNode in xmlNode.ChildNodes)
                {
                    ConvertXMLNodeToTreeNode(childNode, newTreeNode.Nodes);
                }
            }


        }

        private void ClearProfile()
        {
            ClearControls(false);
        }

        public void loadXSDTree()
        {
            tvwXSD.Nodes.Clear();
            XmlDocument doc = new XmlDocument();
            string xsdPath = Path.Combine(Globals.glLibPath, edXSDPath.Text);
            try
            {
                doc.Load(xsdPath);

            }
            catch (Exception err)
            {

            }
            ConvertXMLNodeToTreeNode(doc, tvwXSD.Nodes);
            tvwXSD.Nodes[0].ExpandAll();
        }

        private void rbPickup_CheckedChanged(object sender, EventArgs e)
        {
            SetupReceive();
        }

        private void rbPickupSend_CheckedChanged(object sender, EventArgs e)
        {
            SetupSend();
        }

        private void btnXsdPath_Click(object sender, EventArgs e)
        {
            OpenFileDialog fs = new OpenFileDialog();
            try
            {
                fs.DefaultExt = "*.XSD";
                fs.FileName = edXSDPath.Text;
            }
            catch (Exception ex)
            { }
            fs.ShowDialog();
            edXSDPath.Text = fs.FileName;
        }

        private void btnLoadXSD_Click(object sender, EventArgs e)
        {
            loadXSDTree();
        }

        private void bbAddDTS_Click(object sender, EventArgs e)
        {
            using (IUnitOfWork uow = new UnitOfWork(new SatelliteContext(Globals.connString())))
            {
                bool newRec = false;
                var dts = (uow.DTServices.Find(x => x.D_C == r_id && x.D_P == gP_id && x.D_INDEX == int.Parse(edDTSIndex.Text))).FirstOrDefault();
                if (dts == null)
                {
                    newRec = true;
                    dts = new DTS();
                }

                dts.D_C = r_id;
                dts.D_INDEX = int.Parse(edDTSIndex.Text);
                dts.D_P = gP_id;
                dts.D_FILETYPE = ddlDTSFileType.Text;
                dts.D_SEARCHPATTERN = edDTSSearch.Text;
                dts.D_NEWVALUE = edDTSValue.Text;
                dts.D_QUALIFIER = edDTSQual.Text;
                dts.D_TARGET = edDTSTarget.Text;
                if (rbDTSRecord.Checked)
                    dts.D_DTS = "R";
                if (rbDTSStructure.Checked)
                    dts.D_DTS = "S";
                dts.D_CURRENTVALUE = edDTSCurrentValue.Text;
                if (newRec)
                    uow.DTServices.Add(dts);
                uow.Complete();
            }

        }

        private void btnPickupFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fdPickup = new FolderBrowserDialog();
            fdPickup.SelectedPath = edSendCTCUrl.Text;
            fdPickup.ShowDialog();
            edSendCTCUrl.Text = fdPickup.SelectedPath;
        }

        private void btnReceiveFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = edFTPReceivePath.Text;
            fd.ShowDialog();
            edFTPReceivePath.Text = fd.SelectedPath;
        }

        private void bbTestRx_Click(object sender, EventArgs e)
        {
            //Boolean ssl;
            //if (cbSSL.Checked)
            //    ssl = true;
            //else
            //    ssl = false;
            //txtTestResults.Text = "";
            //txtTestResults.Text = MailModule.CheckMail(edServerAddress.Text, Int16.Parse(edFTPReceivePort.Text), ssl, edReceiveUsername.Text, edReceivePassword.Text);
        }

        private void btnNewProfile_Click(object sender, EventArgs e)
        {
            ClearProfile();
            btnAdd.Text = "Add  >>";
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            using (IUnitOfWork uow = new UnitOfWork(new SatelliteContext(Globals.connString())))
            {

                var profile = uow.Profiles.Get(gP_id);
                if (profile == null)
                {
                    profile = new Profile();
                    profile.P_ID = gP_id == Guid.Empty ? Guid.NewGuid() : gP_id;

                }
                profile.P_REASONCODE = edReasonCode.Text.ToUpper();
                profile.P_EVENTCODE = edEventCode.Text.ToUpper();
                profile.P_C = this.r_id;
                profile.P_DTS = cbDTS.Checked ? "Y" : "N";
                // profile.P_NOTIFY = cbNotify.Checked ? "Y" : "N";
                string sDelivery = string.Empty;
                if (rbSend.Checked)
                {
                    profile.P_DIRECTION = "S";
                    profile.P_SERVER = edSendCTCUrl.Text;
                    profile.P_USERNAME = edSendUsername.Text;
                    profile.P_PASSWORD = edSendPassword.Text;
                    profile.P_SUBJECT = edSendSubject.Text;
                    profile.P_EMAILADDRESS = edSendEmailAddress.Text;

                    foreach (RadioButton rb in gbSendMethod.Controls)
                    {
                        if (rb.Checked)
                        {
                            switch (rb.Name)
                            {

                                case "rbPickupSend":
                                    {
                                        sDelivery = "R";
                                        break;
                                    }
                                case "rbCTCSend":
                                    {
                                        sDelivery = "C";
                                        break;
                                    }
                                case "rbFTPSend":
                                    {
                                        sDelivery = "F";
                                        break;
                                    }
                                case "rbFTPSendDirect":
                                    {
                                        sDelivery = "D";
                                        break;
                                    }
                                case "rbEmailSend":
                                    {
                                        sDelivery = "E";
                                        break;
                                    }
                                case "rbSoapSend":
                                    {
                                        sDelivery = "S";
                                        break;
                                    }
                            }
                        }
                    }
                }
                else
                {
                    profile.P_DIRECTION = "R";
                    profile.P_SERVER = edServerAddress.Text;
                    profile.P_USERNAME = edReceiveUsername.Text;
                    profile.P_PATH = edFTPReceivePath.Text;
                    profile.P_PASSWORD = edReceivePassword.Text;
                    foreach (RadioButton rb in gbReceiveMethod.Controls)
                    {
                        if (rb.Checked)
                        {
                            switch (rb.Name)
                            {

                                case "rbPickup":
                                    {
                                        sDelivery = "R";

                                        break;
                                    }
                                case "rbSendCTC":
                                    {
                                        sDelivery = "C";
                                        break;
                                    }
                                case "rbSendFTP":
                                    {
                                        sDelivery = "F";
                                        break;
                                    }
                                case "rbEmail":
                                    {
                                        sDelivery = "E";
                                        break;
                                    }
                                case "rbReceivePickup":
                                    {
                                        sDelivery = "P";
                                        break;
                                    }
                            }
                        }
                    }
                    profile.P_EMAILADDRESS = edReceiveEmailAddress.Text;
                    profile.P_SUBJECT = edReceiveSubject.Text;
                    profile.P_CUSTOMERCOMPANYNAME = edReceiveCustomerName.Text;
                }
                profile.P_DELIVERY = sDelivery;
                profile.P_PORT = edFTPReceivePort.Text;
                profile.P_DESCRIPTION = edDescription.Text;
                profile.P_RECIPIENTID = edRecipientID.Text;
                profile.P_SENDERID = edSenderID.Text;
                //profile.P_NOTIFYEMAIL = edNotify.Text;
                profile.P_CHARGEABLE = cbChargeable.Checked ? "Y" : "N";
                profile.P_BILLTO = rbCustomer.Checked ? Globals.glCustCode : profile.P_RECIPIENTID;
                profile.P_MSGTYPE = cmbMsgType.Text;
                profile.P_FILETYPE = cmbFileType.Text;
                profile.P_SSL = cbSSL.Checked ? "Y" : "N";
                profile.P_METHOD = edCustomMethod.Text;
                profile.P_SENDEREMAIL = edReceiveSender.Text;
                //profile.P_MAPOPERATION = cmbMapping.SelectedIndex > 0 ? cmbMapping.Text : string.Empty;
                if (!String.IsNullOrEmpty(edXSDPath.Text.Trim()))
                {
                    bool filecopied = false;
                    string fileName = Path.GetFileNameWithoutExtension(edXSDPath.Text);
                    int i = 1;
                    string xsdFile = string.Empty;
                    while (!filecopied)
                    {
                        try
                        {

                            {
                                if (File.Exists(edXSDPath.Text))
                                {
                                    File.Copy(edXSDPath.Text, Path.Combine(Globals.glProfilePath, "Lib", fileName + Path.GetExtension(edXSDPath.Text)));
                                    filecopied = true;
                                    xsdFile = fileName + Path.GetExtension(edXSDPath.Text);
                                }
                                else
                                {
                                    filecopied = true;
                                    xsdFile = edXSDPath.Text;
                                }
                            }

                        }
                        catch (IOException)
                        {
                            i++;
                            fileName = fileName + i;

                        }
                    }
                    profile.P_XSD = xsdFile;
                }
                profile.P_LIBNAME = edLibraryName.Text;
                profile.P_ACTIVE = cbProfileActive.Checked ? "Y" : "N";
                string updateMessage = "Customer Profile Updated";
                if (profile.P_ID == Guid.Empty)
                {
                    profile.P_ID = new Guid();
                    uow.Profiles.Add(profile);
                    updateMessage += ". Rule Created";
                }
                var uowComp = uow.Complete();
                if (uowComp > 0)
                {
                    FillGrid(this.r_id);
                    MessageBox.Show(updateMessage, "Customer Profiles", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }


        }



        private void btnAddParam_Click(object sender, EventArgs e)
        {
            lbParameters.Items.Add(edParameter.Text);
        }

        private void dgProfile_CellContextMenuStripNeeded(object sender, DataGridViewCellContextMenuStripNeededEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;

            if (e.RowIndex == -1 || e.ColumnIndex == -1)
                return;
            bool isActive = true;

            foreach (DataGridViewRow row in dgv.SelectedRows)
            {
                if ((string)row.Cells["Active"].Value == "Y")
                    isActive = true;
                if ((string)row.Cells["Active"].Value == "N")
                    isActive = false;
            }
            if (isActive)
                markInactiveToolStripMenuItem.Text = "Mark InActive";
            else
                markInactiveToolStripMenuItem.Text = "Mark Active";
        }

        public void GetCustomerProfile(int dgRow)
        {
            gP_id = new Guid(dgProfile[0, dgRow].Value.ToString());
            using (IUnitOfWork uow = new UnitOfWork(new SatelliteContext(Globals.connString())))
            {
                var custProfile = uow.Profiles.Get(gP_id);
                if (custProfile != null)
                {

                    edDescription.Text = custProfile.P_DESCRIPTION.Trim() ?? "";
                    edReasonCode.Text = custProfile.P_REASONCODE.Trim() ?? "";
                    edEventCode.Text = custProfile.P_EVENTCODE.Trim() ?? "";
                    edRecipientID.Text = custProfile.P_RECIPIENTID.Trim() ?? "";
                    edServerAddress.Text = custProfile.P_SERVER.Trim() ?? "";
                    edFTPReceivePort.Text = custProfile.P_PORT.Trim() ?? "";
                    edSenderID.Text = custProfile.P_SENDERID.Trim() ?? "";
                    //edNotify.Text = custProfile.P_NOTIFYEMAIL.Trim() ?? "";
                    //cbNotify.Checked = custProfile.P_NOTIFY == "Y" ? true : false;
                    //cmbMapping.Text = !string.IsNullOrEmpty(custProfile.P_MAPOPERATION) ? custProfile.P_MAPOPERATION : "(none selected)";
                    ////TODO Repair XSD Load. 
                    //rtbXSD.Text = dr["P_XSD"].ToString();
                    edXSDPath.Text = string.IsNullOrEmpty(custProfile.P_XSD) ? string.Empty : custProfile.P_XSD.Trim() ?? "";

                    if (!string.IsNullOrEmpty(edXSDPath.Text))
                    {
                        loadXSDTree();
                    }
                    try
                    {

                    }
                    catch (Exception)
                    {

                    }
                    edLibraryName.Text = string.IsNullOrEmpty(custProfile.P_LIBNAME) ? string.Empty : custProfile.P_LIBNAME.Trim();
                    edCustomMethod.Text = string.IsNullOrEmpty(custProfile.P_METHOD) ? string.Empty : custProfile.P_METHOD.Trim();
                    edRecipientID.Text = string.IsNullOrEmpty(custProfile.P_RECIPIENTID) ? string.Empty : custProfile.P_RECIPIENTID.Trim();
                    cmbMsgType.Text = string.IsNullOrEmpty(custProfile.P_MSGTYPE) ? string.Empty : custProfile.P_MSGTYPE.Trim();
                    cbProfileActive.Checked = custProfile.P_ACTIVE == "Y" ? true : false;
                    cbDTS.Checked = custProfile.P_DTS == "N" ? true : false;

                    if (custProfile.P_DIRECTION.Trim() == "R")
                    {
                        rbReceive.Checked = true;
                        rbSend.Checked = false;
                        edServerAddress.Text = string.IsNullOrEmpty(custProfile.P_SERVER) ? string.Empty : custProfile.P_SERVER.Trim();
                        edFTPReceivePath.Text = string.IsNullOrEmpty(custProfile.P_PATH) ? string.Empty : custProfile.P_PATH.Trim();
                        edReceiveUsername.Text = string.IsNullOrEmpty(custProfile.P_USERNAME) ? string.Empty : custProfile.P_USERNAME.Trim();
                        edReceivePassword.Text = string.IsNullOrEmpty(custProfile.P_PASSWORD) ? string.Empty : custProfile.P_PASSWORD.Trim();
                        edReceiveCustomerName.Text = string.IsNullOrEmpty(custProfile.P_CUSTOMERCOMPANYNAME) ? string.Empty : custProfile.P_CUSTOMERCOMPANYNAME.Trim();
                        edReceiveEmailAddress.Text = string.IsNullOrEmpty(custProfile.P_EMAILADDRESS) ? string.Empty : custProfile.P_EMAILADDRESS.Trim();
                        cbSSL.Checked = custProfile.P_SSL == "Y" ? true : false;

                        edReceiveSubject.Text = string.IsNullOrEmpty(custProfile.P_SUBJECT) ? string.Empty : custProfile.P_SUBJECT.Trim();
                        edReceiveSender.Text = string.IsNullOrEmpty(custProfile.P_SENDEREMAIL) ? string.Empty : custProfile.P_SENDEREMAIL.Trim();
                        cmbFileType.Text = string.IsNullOrEmpty(custProfile.P_FILETYPE) ? string.Empty : custProfile.P_FILETYPE.Trim();
                        switch (custProfile.P_DELIVERY)
                        {
                            case "R":
                                {
                                    rbPickup.Checked = true;
                                    break;
                                }
                            case "F":
                                {
                                    rbReceiveFTP.Checked = true;
                                    break;
                                }
                            case "E":
                                {
                                    rbEmail.Checked = true;
                                    break;
                                }
                            case "P":
                                {
                                    rbReceivePickup.Checked = true;
                                    break;
                                }
                            default:
                                {
                                    rbPickup.Checked = true;
                                    break;
                                }
                        }
                        SetupReceive();
                    }
                    else
                    {
                        rbReceive.Checked = false;
                        rbSend.Checked = true;
                        edSendCTCUrl.Text = string.IsNullOrEmpty(custProfile.P_SERVER) ? string.Empty : custProfile.P_SERVER.Trim();
                        edSendFolder.Text = string.IsNullOrEmpty(custProfile.P_PATH) ? string.Empty : custProfile.P_PATH.Trim();
                        edSendUsername.Text = string.IsNullOrEmpty(custProfile.P_USERNAME) ? string.Empty : custProfile.P_USERNAME.Trim();
                        edSendPassword.Text = string.IsNullOrEmpty(custProfile.P_PASSWORD) ? string.Empty : custProfile.P_PASSWORD.Trim();
                        edSendSubject.Text = string.IsNullOrEmpty(custProfile.P_SUBJECT) ? string.Empty : custProfile.P_SUBJECT.Trim();
                        edSendEmailAddress.Text = string.IsNullOrEmpty(custProfile.P_EMAILADDRESS) ? string.Empty : custProfile.P_EMAILADDRESS.Trim();

                        switch (custProfile.P_DELIVERY.Trim())
                        {
                            case "R":
                                {
                                    rbPickupSend.Checked = true;
                                    break;
                                }
                            case "F":
                                {
                                    rbFTPSend.Checked = true;
                                    break;
                                }
                            case "D":
                                {
                                    rbFTPSendDirect.Checked = true;
                                    break;
                                }
                            case "E":
                                {
                                    rbEmailSend.Checked = true;
                                    break;
                                }
                            case "C":
                                {
                                    rbCTCSend.Checked = true;
                                    break;
                                }
                            default:
                                {
                                    rbPickupSend.Checked = true;
                                    break;
                                }
                        }
                        SetupSend();
                    }
                    btnAdd.Text = "&Update >>";
                }
            }
            FillDTSGrid(gP_id);

        }

        private void dgProfile_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            GetCustomerProfile(e.RowIndex);
        }

        private void dgProfile_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (dgProfile.Rows[e.RowIndex].Cells["Direction"].Value.ToString() == "R")
            {
                dgProfile.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LawnGreen;
            }
            if (dgProfile.Rows[e.RowIndex].Cells["Direction"].Value.ToString() == "S")
            {
                dgProfile.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightSkyBlue;
            }
            if (dgProfile.Rows[e.RowIndex].Cells["Active"].Value.ToString() == "N")
            {
                dgProfile.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.LightGray;
            }
        }

        private void dgProfile_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex != -1 && e.RowIndex != -1 && e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                DataGridViewCell c = (sender as DataGridView)[e.ColumnIndex, e.RowIndex];
                if (!c.Selected)
                {
                    c.DataGridView.ClearSelection();
                    c.DataGridView.CurrentCell = c;
                    c.Selected = true;
                }
            }
        }

        private void markInactiveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int rowIndex = dgProfile.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            DataGridViewRow row = dgProfile.Rows[rowIndex];
            String strActiveToggle = "";
            switch (markInactiveToolStripMenuItem.Text)
            {
                case "Mark InActive":
                    strActiveToggle = "N";
                    break;
                case "Mark Active":
                    strActiveToggle = "Y";
                    break;
            }
            using (IUnitOfWork uow = new UnitOfWork(new SatelliteContext(Globals.connString())))
            {
                var prof = uow.Profiles.Get(gP_id);
                if (prof != null)
                {
                    prof.P_ACTIVE = strActiveToggle;
                }
                uow.Complete();
            }
            FillGrid(this.r_id);
        }

        private void duplicateProfileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int rowIndex = dgProfile.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            DataGridViewRow row = dgProfile.Rows[rowIndex];
            Guid p_id = new Guid(dgProfile[0, rowIndex].Value.ToString());
            using (IUnitOfWork uow = new UnitOfWork(new SatelliteContext(Globals.connString())))
            {
                var profile = uow.Profiles.Get(p_id);
                if (profile != null)
                {
                    Profile newProfile = profile.DeepClone();
                    newProfile.P_DESCRIPTION += " - Duplicate";
                    newProfile.P_ID = Guid.NewGuid();
                    uow.Profiles.Add(newProfile);
                    uow.Complete();
                }
            }
            FillGrid(this.r_id);
        }

        private void deleteProfileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int rowIndex = dgProfile.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            DataGridViewRow row = dgProfile.Rows[rowIndex];
            Guid p_id = new Guid(dgProfile[0, rowIndex].Value.ToString());
            DialogResult drDelete = MessageBox.Show("Are you sure you wish to delete this rule?", "Delete from Customer Profile", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (drDelete == DialogResult.Yes)
            {
                deleteProfile(p_id);
            }
        }

        private void dgProfile_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            Guid p_id = new Guid(dgProfile[0, e.Row.Index].Value.ToString());

            DialogResult drDelete = MessageBox.Show("Are you sure you wish to delete this rule?", "Delete from Customer Profile", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (drDelete == DialogResult.Yes)
            {
                deleteProfile(p_id);
            }
        }

        private void deleteProfile(Guid p_id)
        {

            using (IUnitOfWork uow = new UnitOfWork(new SatelliteContext(Globals.connString())))
            {
                var toBeDeleted = uow.Profiles.Get(p_id);
                if (toBeDeleted != null)
                {
                    uow.Profiles.Remove(toBeDeleted);
                    uow.Complete();
                }
            }
            FillGrid(this.r_id);
            MessageBox.Show("Client rule deleted", "Customer profile updated", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        private void gbDirection_Enter(object sender, EventArgs e)
        {

        }

        private void rbReceive_CheckedChanged(object sender, EventArgs e)
        {
            if (rbSend.Checked)
            {
                gbReceiveOptions.Enabled = false;
                gbSendOptions.Enabled = true;
            }
            else
            {
                gbReceiveOptions.Enabled = true;
                gbSendOptions.Enabled = false;
            }
        }

        private void cmProfiles_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }

        private void btnCSV_Click(object sender, EventArgs e)
        {
            using (frmCSVDetails cSVDetails = new frmCSVDetails())
            {
                cSVDetails.Show();
            }
        }
    }
}
