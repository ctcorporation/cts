﻿using CTS_Satellite.Models;

namespace CTS_Satellite.Repository
{
    public class CustomerProfileRepository : GenericRepository<Profile>, ICustomerProfileRepository
    {

        public CustomerProfileRepository(SatelliteContext context)
            : base(context)
        {

        }

        public SatelliteContext SatelliteContext
        {
            get
            {
                return Context as SatelliteContext;
            }
        }
    }

}
