﻿using CTS_Satellite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTS_Satellite.Repository
{
    public class ProcErrorsRepository : GenericRepository<Processing_Errors>, IProcErrorsRepository
    {
        public ProcErrorsRepository(SatelliteContext context)
            :base(context)
        {

        }

        public SatelliteContext SatelliteContext
        {
            get
            {
                return Context as SatelliteContext;
            }
        }
    }
}
