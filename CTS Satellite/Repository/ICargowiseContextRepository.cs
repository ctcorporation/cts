﻿using CTS_Satellite.Models;

namespace CTS_Satellite.Repository
{
    public interface ICargowiseContextRepository : IGenericRepository<CargowiseContext>
    {
        SatelliteContext SatelliteContext { get; }
    }
}