﻿using CTS_Satellite.Models;

namespace CTS_Satellite.Repository
{
    public interface IFileDescriptionRepository : IGenericRepository<FileDescription>
    {
        SatelliteContext SatelliteContext { get; }
    }
}