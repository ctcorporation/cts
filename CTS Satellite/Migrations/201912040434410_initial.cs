﻿namespace CTS_Satellite.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cargowise_Enums",
                c => new
                {
                    CW_ID = c.Guid(nullable: false, identity: true, defaultValueSql: "newid()"),
                    CW_ENUMTYPE = c.String(maxLength: 50, fixedLength: true, unicode: false),
                    CW_ENUM = c.String(maxLength: 100, fixedLength: true),
                    CW_MAPVALUE = c.String(maxLength: 100, fixedLength: true),
                    CW_EXTRAVALUE = c.String(maxLength: 20, unicode: false),
                })
                .PrimaryKey(t => t.CW_ID);

            CreateTable(
                "dbo.CargowiseContext",
                c => new
                {
                    CC_ID = c.Guid(nullable: false, identity: true, defaultValueSql: "newid()"),
                    CC_Context = c.String(maxLength: 50, unicode: false),
                    CC_Description = c.String(unicode: false),
                })
                .PrimaryKey(t => t.CC_ID);

            CreateTable(
                "dbo.Customer",
                c => new
                {
                    C_ID = c.Guid(nullable: false, identity: true, defaultValueSql: "newid()"),
                    C_NAME = c.String(maxLength: 50, unicode: false),
                    C_IS_ACTIVE = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
                    C_ON_HOLD = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
                    C_PATH = c.String(maxLength: 80, unicode: false),
                    C_FTP_CLIENT = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
                    C_CODE = c.String(maxLength: 15, unicode: false),
                    C_TRIAL = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
                    C_TRIALSTART = c.DateTime(),
                    C_TRIALEND = c.DateTime(),
                    C_SHORTNAME = c.String(maxLength: 10, fixedLength: true),
                })
                .PrimaryKey(t => t.C_ID);

            CreateTable(
                "dbo.CustomMapping",
                c => new
                {
                    CF_ID = c.Guid(nullable: false, identity: true, defaultValueSql: "newid()"),
                    CF_NAME = c.String(maxLength: 50, unicode: false),
                    CF_P = c.Guid(),
                    CF_ColumnFrom = c.String(maxLength: 50, unicode: false),
                    CF_ColumnTo = c.String(maxLength: 50, unicode: false),
                    CF_TableName = c.String(maxLength: 20, unicode: false),
                })
                .PrimaryKey(t => t.CF_ID);

            CreateTable(
                "dbo.CustomRelations",
                c => new
                {
                    CR_ID = c.Guid(nullable: false, identity: true, defaultValueSql: "newid()"),
                    CR_ParentTable = c.String(maxLength: 20, unicode: false),
                    CR_ChildTable = c.String(maxLength: 20, unicode: false),
                    CR_ParentColumn = c.String(maxLength: 50, unicode: false),
                    CR_ChildColumn = c.String(maxLength: 50, unicode: false),
                })
                .PrimaryKey(t => t.CR_ID);

            CreateTable(
                "dbo.DTS",
                c => new
                {
                    D_ID = c.Guid(nullable: false, identity: true, defaultValueSql: "newid()"),
                    D_C = c.Guid(),
                    D_INDEX = c.Int(),
                    D_FINALPROCESSING = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
                    D_P = c.Guid(),
                    D_FILETYPE = c.String(maxLength: 3, fixedLength: true, unicode: false),
                    D_DTSTYPE = c.String(maxLength: 1, fixedLength: true, unicode: false),
                    D_DTS = c.String(maxLength: 1, fixedLength: true, unicode: false),
                    D_SEARCHPATTERN = c.String(unicode: false),
                    D_NEWVALUE = c.String(unicode: false),
                    D_QUALIFIER = c.String(unicode: false),
                    D_TARGET = c.String(unicode: false),
                    D_CURRENTVALUE = c.String(unicode: false),
                })
                .PrimaryKey(t => t.D_ID);

            CreateTable(
                "dbo.Organisations",
                c => new
                {
                    OR_ID = c.Guid(nullable: false, identity: true, defaultValueSql: "newid()"),
                    OR_CODE = c.String(maxLength: 20, fixedLength: true),
                    OR_Name = c.String(maxLength: 100, fixedLength: true),
                    OR_Address1 = c.String(maxLength: 100, fixedLength: true),
                    OR_Address2 = c.String(maxLength: 100, fixedLength: true),
                    OR_Address3 = c.String(maxLength: 100, fixedLength: true),
                    OR_PostCode = c.String(maxLength: 10, fixedLength: true),
                    OR_Suburb = c.String(maxLength: 50, fixedLength: true),
                    OR_State = c.String(maxLength: 10, fixedLength: true),
                    OR_MapValue = c.String(maxLength: 200, unicode: false),
                    OR_Type = c.String(maxLength: 50, unicode: false),
                    OR_PortOfLoading = c.String(maxLength: 5, fixedLength: true, unicode: false),
                    OR_PortOfDischarge = c.String(maxLength: 5, fixedLength: true, unicode: false),
                    OR_ShortCode = c.String(maxLength: 50, unicode: false),
                    OR_AirPortOfLoading = c.String(maxLength: 5, fixedLength: true, unicode: false),
                    OR_AirPortOfOrigin = c.String(maxLength: 5, fixedLength: true, unicode: false),
                    OR_SeaPortOfOrigin = c.String(maxLength: 5, fixedLength: true, unicode: false),
                })
                .PrimaryKey(t => t.OR_ID);

            CreateTable(
                "dbo.Processing_Errors",
                c => new
                {
                    E_PK = c.Guid(nullable: false, identity: true, defaultValueSql: "newid()"),
                    E_SENDERID = c.String(maxLength: 15, unicode: false),
                    E_RECIPIENTID = c.String(maxLength: 15, unicode: false),
                    E_PROCDATE = c.DateTime(),
                    E_FILENAME = c.String(unicode: false),
                    E_ERRORDESC = c.String(unicode: false),
                    E_ERRORCODE = c.String(maxLength: 10, fixedLength: true),
                    E_P = c.Guid(),
                    E_IGNORE = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
                })
                .PrimaryKey(t => t.E_PK);

            CreateTable(
                "dbo.PROFILE",
                c => new
                {
                    P_ID = c.Guid(nullable: false, identity: true, defaultValueSql: "newid()"),
                    P_C = c.Guid(),
                    P_REASONCODE = c.String(maxLength: 3, fixedLength: true, unicode: false),
                    P_SERVER = c.String(unicode: false),
                    P_USERNAME = c.String(unicode: false),
                    P_PASSWORD = c.String(unicode: false),
                    P_DELIVERY = c.String(maxLength: 1, fixedLength: true, unicode: false),
                    P_PORT = c.String(maxLength: 10, fixedLength: true, unicode: false),
                    P_DESCRIPTION = c.String(unicode: false),
                    P_PATH = c.String(maxLength: 100, unicode: false),
                    P_DIRECTION = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
                    P_LIBNAME = c.String(maxLength: 50, unicode: false),
                    P_MESSAGETYPE = c.String(maxLength: 3, fixedLength: true, unicode: false),
                    P_RECIPIENTID = c.String(maxLength: 15, unicode: false),
                    P_MSGTYPE = c.String(maxLength: 20, fixedLength: true, unicode: false),
                    P_CHARGEABLE = c.String(maxLength: 1, fixedLength: true, unicode: false),
                    P_BILLTO = c.String(maxLength: 15, fixedLength: true, unicode: false),
                    P_SENDERID = c.String(maxLength: 15, unicode: false),
                    P_DTS = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
                    P_EMAILADDRESS = c.String(maxLength: 100, unicode: false),
                    P_ACTIVE = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
                    P_SSL = c.String(maxLength: 1, fixedLength: true, unicode: false),
                    P_SENDEREMAIL = c.String(maxLength: 100, unicode: false),
                    P_SUBJECT = c.String(maxLength: 100, unicode: false),
                    P_FILETYPE = c.String(maxLength: 5, fixedLength: true, unicode: false),
                    P_MESSAGEDESCR = c.String(maxLength: 20, unicode: false),
                    P_EVENTCODE = c.String(maxLength: 3, fixedLength: true, unicode: false),
                    P_CUSTOMERCOMPANYNAME = c.String(maxLength: 50, unicode: false),
                    P_GROUPCHARGES = c.String(maxLength: 1, fixedLength: true, unicode: false),
                    P_XSD = c.String(maxLength: 50, unicode: false),
                    P_PARAMLIST = c.String(unicode: false),
                    P_METHOD = c.String(maxLength: 50, unicode: false),
                    P_PCTCNODE = c.Guid(),
                    P_NOTIFYEMAIL = c.String(maxLength: 100, unicode: false),
                })
                .PrimaryKey(t => t.P_ID);

            CreateTable(
                "dbo.TODO",
                c => new
                {
                    L_ID = c.Guid(nullable: false, identity: true, defaultValueSql: "newid()"),
                    L_P = c.Guid(),
                    L_FILENAME = c.String(unicode: false),
                    L_LASTRESULT = c.String(unicode: false),
                    L_DATE = c.DateTime(),
                })
                .PrimaryKey(t => t.L_ID);

            CreateTable(
                "dbo.Transaction_Log",
                c => new
                {
                    X_ID = c.Guid(nullable: false, identity: true, defaultValueSql: "newid()"),

                    X_P = c.Guid(),
                    X_FILENAME = c.String(maxLength: 150, unicode: false),
                    X_DATE = c.DateTime(),
                    X_SUCCESS = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
                    X_C = c.Guid(),
                    X_LASTRESULT = c.String(maxLength: 120, unicode: false),
                })
                .PrimaryKey(t => t.X_ID);

            CreateTable(
                "dbo.Transactions",
                c => new
                {
                    T_ID = c.Guid(nullable: false, identity: true, defaultValueSql: "newid()"),
                    T_C = c.Guid(),
                    T_P = c.Guid(),
                    T_DATETIME = c.DateTime(),
                    T_FILENAME = c.String(maxLength: 100, unicode: false),
                    T_TRIAL = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
                    T_INVOICED = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
                    T_INVOICEDATE = c.DateTime(),
                    T_MSGTYPE = c.String(maxLength: 10, fixedLength: true, unicode: false),
                    T_BILLTO = c.String(maxLength: 15, fixedLength: true, unicode: false),
                    T_DIRECTION = c.String(maxLength: 1, fixedLength: true, unicode: false),
                    T_CHARGEABLE = c.String(maxLength: 1, fixedLength: true, unicode: false),
                    T_REF1 = c.String(maxLength: 50),
                    T_REF2 = c.String(maxLength: 50),
                    T_REF3 = c.String(maxLength: 50),
                    T_ARCHIVE = c.String(maxLength: 20),
                    T_REF1TYPE = c.String(maxLength: 10, fixedLength: true, unicode: false),
                    T_REF2TYPE = c.String(maxLength: 10, fixedLength: true, unicode: false),
                    T_REF3TYPE = c.String(maxLength: 10, fixedLength: true, unicode: false),
                })
                .PrimaryKey(t => t.T_ID);

        }

        public override void Down()
        {
            DropTable("dbo.Transactions");
            DropTable("dbo.Transaction_Log");
            DropTable("dbo.TODO");
            DropTable("dbo.PROFILE");
            DropTable("dbo.Processing_Errors");
            DropTable("dbo.Organisations");
            DropTable("dbo.DTS");
            DropTable("dbo.CustomRelations");
            DropTable("dbo.CustomMapping");
            DropTable("dbo.Customer");
            DropTable("dbo.CargowiseContext");
            DropTable("dbo.Cargowise_Enums");
        }
    }
}
