﻿using CTS_Satellite.DTO;
using CTS_Satellite.Models;
using CTS_Satellite.Resources;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace CTS_Satellite
{
    public partial class frmFieldMapping : Form
    {
        Guid id;
        BindingSource bsCustomMapping = new BindingSource();
        public frmFieldMapping()
        {
            InitializeComponent();
        }

        private void bbClose_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void frmFieldMapping_Load(object sender, EventArgs e)
        {
            id = new Guid();
            FillContextList();
            LoadMappingList();

        }

        private void LoadMappingList()
        {
            using (IUnitOfWork uow = new UnitOfWork(new SatelliteContext(Globals.ConnString.ConnString)))
            {
                var fieldMappings = uow.CustomMaps.GetAll().OrderBy(x => x.CF_NAME).ToList();
                if (fieldMappings.Count > 0)
                {
                    bsCustomMapping.DataSource = new SortableBindingList<CustomMapping>(fieldMappings);
                    dgFieldMapping.DataSource = bsCustomMapping;
                }
            }

        }

        private void FillContextList()
        {
            using (IUnitOfWork uow = new UnitOfWork(new SatelliteContext(Globals.ConnString.ConnString)))
            {
                var enumList = uow.Enums.Find(x => x.CW_ENUMTYPE == "RecordType").OrderBy(x => x.CW_ENUM).ThenBy(x => x.CW_MAPVALUE).ToList();
                if (enumList.Count > 0)
                {
                    enumList.Insert(0, new Cargowise_Enums { CW_ID = Guid.Empty, CW_ENUM = "(none selected)" });
                    cmbContext.DataSource = enumList;
                    cmbContext.DisplayMember = "CW_ENUM";
                    cmbContext.ValueMember = "CW_ID";

                }
            }
        }


        private void dgFieldMapping_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            edCustomField.Text = dgFieldMapping[0, e.RowIndex].Value.ToString();
            edMapToField.Text = dgFieldMapping[1, e.RowIndex].Value.ToString();
            cmbDataType.Text = dgFieldMapping[2, e.RowIndex].Value.ToString();
            cmbContext.SelectedValue = dgFieldMapping[4, e.RowIndex].Value;
            id = (Guid)dgFieldMapping[5, e.RowIndex].Value;
            btnAddMapping.Text = "&Update";

        }

        private void btnAddMapping_Click(object sender, EventArgs e)
        {
            if (cmbContext.Text != "(none slected)")
            {
                using (IUnitOfWork uow = new UnitOfWork(new SatelliteContext(Globals.ConnString.ConnString)))
                {
                    var newMap = new CustomMapping();
                    if (btnAddMapping.Text == "&Update")
                    {
                        newMap = uow.CustomMaps.Get(id);

                    }
                    newMap.CF_ColumnFrom = edCustomField.Text;
                    newMap.CF_ColumnTo = edMapToField.Text;
                    newMap.CF_TableName = cmbContext.Text;
                    if (btnAddMapping.Text == "&Update")
                    {
                        uow.CustomMaps.Add(newMap);
                    }
                    if (uow.Complete() > 0)
                    {
                        MessageBox.Show("Custom Mapping Updated", "Custom Mapping", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        id = Guid.Empty;
                        btnAddMapping.Text = "&Add";
                    }
                }
            }



        }
    }
}
