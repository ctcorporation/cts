﻿using CTS_Satellite.Models;

namespace CTS_Satellite.Repository
{
    public class HeartBeatRepository : GenericNodeRepository<HeartBeats>, IHeartBeatRepository
    {
        public HeartBeatRepository(NodeDataContext context)
            : base(context)
        {

        }

        public NodeDataContext NodeDataContext
        {
            get
            {
                return Context as NodeDataContext;
            }
        }
    }
}
