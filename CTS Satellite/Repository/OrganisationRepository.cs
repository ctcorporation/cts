﻿using CTS_Satellite.Models;

namespace CTS_Satellite.Repository
{
    public class OrganisationRepository : GenericRepository<Organisation>, IOrganisationRepository
    {
        public OrganisationRepository(SatelliteContext context)
            : base(context)
        {

        }

        public SatelliteContext SatelliteContext
        {
            get
            {
                return Context as SatelliteContext;
            }

        }
    }
}
