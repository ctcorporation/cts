﻿using CTS_Satellite.Models;
using CTS_Satellite.Repository;

using System;
using System.Data.Entity.Validation;

namespace CTS_Satellite.DTO
{
    public class UnitOfWork : IUnitOfWork
    {
        #region members
        private SatelliteContext _context;
        private NodeDataContext _nodeContext;
        private bool disposed = false;
        private string _errorMessage = string.Empty;
        #endregion

        #region properties
        public ICargowiseContextRepository CWContext
        {
            get;
            private set;
        }
        public IDTSRepository DTServices
        {
            get;
            private set;
        }
        public IHeartBeatRepository HBeats
        {
            get;
            private set;
        }

        public INodeCustRepository NodeCust
        {
            get;
            private set;
        }
        public ITransactionRepository Transactions
        {
            get;
            private set;
        }
        public IProcErrorsRepository ProcErrors
        {
            get;
            private set;
        }
        public ICustomerProfileRepository Profiles
        {
            get;
            private set;
        }

        public ICustomerRepository Customers
        {
            get;
            private set;
        }

        public ICustomMappingRepository CustomMaps
        {
            get;
            private set;
        }

        public IEnumRepository Enums
        {
            get;
            private set;
        }
        public IOrganisationRepository Organisations
        {
            get;
            private set;
        }

        public IFileDescriptionRepository FileDescriptions
        {
            get;
            private set;
        }
        #endregion

        #region Constructors
        public UnitOfWork(SatelliteContext context)
        {
            _context = context;
            AddRepos();
        }
        public UnitOfWork(NodeDataContext context)
        {
            _nodeContext = context;
            AddNodeRepos();
        }


        #endregion

        #region Methods
        private void AddNodeRepos()
        {
            HBeats = new HeartBeatRepository(_nodeContext);
            NodeCust = new NodeCustRepository(_nodeContext);

        }
        public void AddRepos()
        {
            ProcErrors = new ProcErrorsRepository(_context);
            Profiles = new CustomerProfileRepository(_context);
            Customers = new CustomerRepository(_context);
            CustomMaps = new CustomMappingRepository(_context);
            Organisations = new OrganisationRepository(_context);
            FileDescriptions = new FileDescriptionRepository(_context);
            Enums = new EnumRepository(_context);
            Transactions = new TransactionRepository(_context);
            DTServices = new DTSRepository(_context);
            CWContext = new CargowiseContextRepository(_context);

        }
        public int Complete()
        {
            try
            {
                return _context.SaveChanges();

            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        _errorMessage += string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(_errorMessage, dbEx);
            }

        }

        public int NodeComplete()
        {
            try
            {
                return _nodeContext.SaveChanges();

            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        _errorMessage += string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(_errorMessage, dbEx);
            }

        }


        #endregion

        #region Helpers
        public virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                try
                {
                    if (_context!=null)
                    _context.Dispose();
                    if (_nodeContext != null)
                        _nodeContext.Dispose();
                }
                catch(Exception)
                {

                }
                
            }
        }
        public void Dispose()
        {
            if (!disposed)
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
        }
        #endregion

    }
}
