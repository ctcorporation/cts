﻿namespace CTS_Satellite.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class FileDescription : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FileDescription",
                c => new
                {
                    PC_ID = c.Guid(nullable: false, identity: true, defaultValueSql: "newid()"),
                    PC_P = c.Guid(),
                    PC_FileName = c.String(maxLength: 50),
                    PC_HasHeader = c.Boolean(),
                    PC_Delimiter = c.String(maxLength: 1),
                    PC_Fieldcount = c.Int(),
                    PC_FirstFieldName = c.String(maxLength: 50),
                    PC_LastFieldName = c.String(maxLength: 50),
                    PC_Quotations = c.Boolean(),
                    PC_HeaderStart = c.Int(),
                    PC_DataStart = c.Int(),
                })
                .PrimaryKey(t => t.PC_ID);

        }

        public override void Down()
        {
            DropTable("dbo.FileDescription");
        }
    }
}
