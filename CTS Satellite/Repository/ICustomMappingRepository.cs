﻿using CTS_Satellite.Models;

namespace CTS_Satellite.Repository
{
    public interface ICustomMappingRepository : IGenericRepository<CustomMapping>
    {
        SatelliteContext SatelliteContext { get; }
    }
}