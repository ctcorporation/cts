﻿using CTS_Satellite.Models;

namespace CTS_Satellite.Repository
{
    public class DTSRepository : GenericRepository<DTS>, IDTSRepository
    {
        public DTSRepository(SatelliteContext context)
            : base(context)
        {

        }

        public SatelliteContext SatelliteContext
        {
            get { return Context as SatelliteContext; }
        }

    }
}
