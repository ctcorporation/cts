﻿namespace CTS_Satellite
{
    partial class frmFieldMapping
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bbClose = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dgFieldMapping = new System.Windows.Forms.DataGridView();
            this.gbEditFieldListing = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.edCustomField = new System.Windows.Forms.TextBox();
            this.edMapToField = new System.Windows.Forms.TextBox();
            this.cmbDataType = new System.Windows.Forms.ComboBox();
            this.cmbContext = new System.Windows.Forms.ComboBox();
            this.btnAddMapping = new System.Windows.Forms.Button();
            this.CustomFieldName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MapToField = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CargowiseContext = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CW_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cfid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgFieldMapping)).BeginInit();
            this.gbEditFieldListing.SuspendLayout();
            this.SuspendLayout();
            // 
            // bbClose
            // 
            this.bbClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bbClose.Location = new System.Drawing.Point(769, 370);
            this.bbClose.Name = "bbClose";
            this.bbClose.Size = new System.Drawing.Size(75, 23);
            this.bbClose.TabIndex = 0;
            this.bbClose.Text = "&Close";
            this.bbClose.UseVisualStyleBackColor = true;
            this.bbClose.Click += new System.EventHandler(this.bbClose_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Field Mapping List";
            // 
            // dgFieldMapping
            // 
            this.dgFieldMapping.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgFieldMapping.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgFieldMapping.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CustomFieldName,
            this.MapToField,
            this.DataType,
            this.CargowiseContext,
            this.CW_ID,
            this.cfid});
            this.dgFieldMapping.Location = new System.Drawing.Point(20, 47);
            this.dgFieldMapping.Name = "dgFieldMapping";
            this.dgFieldMapping.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgFieldMapping.Size = new System.Drawing.Size(823, 188);
            this.dgFieldMapping.TabIndex = 2;

            this.dgFieldMapping.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgFieldMapping_CellDoubleClick);
            // 
            // gbEditFieldListing
            // 
            this.gbEditFieldListing.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbEditFieldListing.Controls.Add(this.btnAddMapping);
            this.gbEditFieldListing.Controls.Add(this.cmbContext);
            this.gbEditFieldListing.Controls.Add(this.cmbDataType);
            this.gbEditFieldListing.Controls.Add(this.edMapToField);
            this.gbEditFieldListing.Controls.Add(this.edCustomField);
            this.gbEditFieldListing.Controls.Add(this.label5);
            this.gbEditFieldListing.Controls.Add(this.label4);
            this.gbEditFieldListing.Controls.Add(this.label3);
            this.gbEditFieldListing.Controls.Add(this.label2);
            this.gbEditFieldListing.Location = new System.Drawing.Point(20, 241);
            this.gbEditFieldListing.Name = "gbEditFieldListing";
            this.gbEditFieldListing.Size = new System.Drawing.Size(823, 113);
            this.gbEditFieldListing.TabIndex = 3;
            this.gbEditFieldListing.TabStop = false;
            this.gbEditFieldListing.Text = "Add/Edit Custom Field Mapping";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Customer Field Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(222, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Map to Field";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(436, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Data Type";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(614, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Field Context";
            // 
            // edCustomField
            // 
            this.edCustomField.Location = new System.Drawing.Point(15, 48);
            this.edCustomField.Name = "edCustomField";
            this.edCustomField.Size = new System.Drawing.Size(179, 20);
            this.edCustomField.TabIndex = 4;
            // 
            // edMapToField
            // 
            this.edMapToField.Location = new System.Drawing.Point(225, 48);
            this.edMapToField.Name = "edMapToField";
            this.edMapToField.Size = new System.Drawing.Size(179, 20);
            this.edMapToField.TabIndex = 5;
            // 
            // cmbDataType
            // 
            this.cmbDataType.FormattingEnabled = true;
            this.cmbDataType.Items.AddRange(new object[] {
            "(none selected)",
            "Date Time",
            "Decimal",
            "Number",
            "String"});
            this.cmbDataType.Location = new System.Drawing.Point(439, 47);
            this.cmbDataType.Name = "cmbDataType";
            this.cmbDataType.Size = new System.Drawing.Size(121, 21);
            this.cmbDataType.TabIndex = 6;
            // 
            // cmbContext
            // 
            this.cmbContext.FormattingEnabled = true;
            this.cmbContext.Location = new System.Drawing.Point(617, 48);
            this.cmbContext.Name = "cmbContext";
            this.cmbContext.Size = new System.Drawing.Size(200, 21);
            this.cmbContext.TabIndex = 7;
            // 
            // btnAddMapping
            // 
            this.btnAddMapping.Location = new System.Drawing.Point(742, 84);
            this.btnAddMapping.Name = "btnAddMapping";
            this.btnAddMapping.Size = new System.Drawing.Size(75, 23);
            this.btnAddMapping.TabIndex = 8;
            this.btnAddMapping.Text = "&Add";
            this.btnAddMapping.UseVisualStyleBackColor = true;
            this.btnAddMapping.Click += new System.EventHandler(this.btnAddMapping_Click);
            // 
            // CustomFieldName
            // 
            this.CustomFieldName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CustomFieldName.DataPropertyName = "CF_CUSTOMFIELDNAME";
            this.CustomFieldName.HeaderText = "Customer Field Name";
            this.CustomFieldName.Name = "CustomFieldName";
            // 
            // MapToField
            // 
            this.MapToField.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.MapToField.DataPropertyName = "CF_CWFIELDNAME";
            this.MapToField.HeaderText = "Map to Field";
            this.MapToField.Name = "MapToField";
            // 
            // DataType
            // 
            this.DataType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.DataType.DataPropertyName = "CF_DATATYPE";
            this.DataType.HeaderText = "Data Type";
            this.DataType.Name = "DataType";
            this.DataType.Width = 82;
            // 
            // CargowiseContext
            // 
            this.CargowiseContext.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.CargowiseContext.DataPropertyName = "CW_ENUM";
            this.CargowiseContext.HeaderText = "Context";
            this.CargowiseContext.Name = "CargowiseContext";
            this.CargowiseContext.Width = 68;
            // 
            // CW_ID
            // 
            this.CW_ID.DataPropertyName = "CF_CW";
            this.CW_ID.HeaderText = "cw_id";
            this.CW_ID.Name = "CW_ID";
            this.CW_ID.Visible = false;
            // 
            // cfid
            // 
            this.cfid.DataPropertyName = "CF_ID";
            this.cfid.HeaderText = "ID";
            this.cfid.Name = "cfid";
            this.cfid.Visible = false;
            // 
            // frmFieldMapping
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(856, 405);
            this.Controls.Add(this.gbEditFieldListing);
            this.Controls.Add(this.dgFieldMapping);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bbClose);
            this.Name = "frmFieldMapping";
            this.Text = "Custom Field Mappings";
            this.Load += new System.EventHandler(this.frmFieldMapping_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgFieldMapping)).EndInit();
            this.gbEditFieldListing.ResumeLayout(false);
            this.gbEditFieldListing.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bbClose;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgFieldMapping;
        private System.Windows.Forms.GroupBox gbEditFieldListing;
        private System.Windows.Forms.Button btnAddMapping;
        private System.Windows.Forms.ComboBox cmbContext;
        private System.Windows.Forms.ComboBox cmbDataType;
        private System.Windows.Forms.TextBox edMapToField;
        private System.Windows.Forms.TextBox edCustomField;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn CustomFieldName;
        private System.Windows.Forms.DataGridViewTextBoxColumn MapToField;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataType;
        private System.Windows.Forms.DataGridViewTextBoxColumn CargowiseContext;
        private System.Windows.Forms.DataGridViewTextBoxColumn CW_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn cfid;
    }
}