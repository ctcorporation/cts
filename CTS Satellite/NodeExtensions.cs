﻿using System;
using System.Globalization;

namespace CTS_Satellite
{
    class NodeExtensions
    {

    }
    public static class StringEx
    {
        public static string Truncate(this string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) return value;
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }
    }

    public static class CellDatetoDateTime
    {
        public static string CheckDate(this string value)
        {
            DateTime res;
            if (DateTime.TryParse(value, out res))
            {
                return res.ToString();
            }
            else
                return null;
        }
    }
    public static class DateTimeEx
    {
        public static DateTime ParseDateEMO(this DateTime value, string formatString)
        {
            string dateTimeStr = value.ToString(formatString);

            return DateTime.ParseExact(dateTimeStr, "yyyy-MM-ddTHH:mm:ss.ffzzz", CultureInfo.InvariantCulture);
        }
    }
}
