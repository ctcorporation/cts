namespace CTS_Satellite.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CustomRelation
    {
        [Key]
        public Guid CR_ID { get; set; }

        [StringLength(20)]
        public string CR_ParentTable { get; set; }

        [StringLength(20)]
        public string CR_ChildTable { get; set; }

        [StringLength(50)]
        public string CR_ParentColumn { get; set; }

        [StringLength(50)]
        public string CR_ChildColumn { get; set; }
    }
}
