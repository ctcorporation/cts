﻿using CTS_Satellite.DTO;
using CTS_Satellite.Models;
using CTS_Satellite.Resources;
using System;
using System.Data;

using System.Linq;
using System.Windows.Forms;

namespace CTS_Satellite
{
    public partial class frmEnums : Form
    {
        public Guid cw_ID;
        public BindingSource bsEnums;
        public frmEnums()
        {
            InitializeComponent();
        }

        private void frmEnums_Load(object sender, EventArgs e)
        {
            LoadData();
        }


        private void LoadData()
        {
            using (IUnitOfWork uow = new UnitOfWork(new SatelliteContext(Globals.ConnString.ConnString)))
            {
                var enums = uow.Enums.GetAll().OrderBy(x => x.CW_ENUMTYPE).ThenBy(x => x.CW_MAPVALUE).ToList();
                if (enums.Count > 0)
                {
                    bsEnums = new BindingSource();
                    bsEnums.DataSource = new SortableBindingList<Cargowise_Enums>(enums);
                    dgEnums.DataSource = bsEnums;
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(edCargowiseValue.Text) && !string.IsNullOrEmpty(edCustomValue.Text) && !string.IsNullOrEmpty(edDatatype.Text))
            {
                using (IUnitOfWork uow = new UnitOfWork(new SatelliteContext(Globals.ConnString.ConnString)))
                {
                    var enums = new Cargowise_Enums();

                    {
                        enums = uow.Enums.Get(cw_ID);
                        if (enums == null)
                        {
                            enums = new Cargowise_Enums();

                        }
                    }

                    enums.CW_ENUM = edCargowiseValue.Text;
                    enums.CW_ENUMTYPE = edDatatype.Text;
                    enums.CW_MAPVALUE = edCustomValue.Text;
                    if (cw_ID == Guid.Empty)
                    {

                        uow.Enums.Add(enums);

                    }
                    if (uow.Complete() > 0)
                    {
                        MessageBox.Show("Enum Values updated", "Custom Enum Mapping", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LoadData();
                    }

                }

            }

        }

        private void dgEnums_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            cw_ID = (Guid)dgEnums[0, e.RowIndex].Value;

            edCargowiseValue.Text = dgEnums["CWValue", e.RowIndex].Value.ToString();
            edCustomValue.Text = dgEnums["Custom", e.RowIndex].Value.ToString();
            edDatatype.Text = dgEnums["Type", e.RowIndex].Value.ToString();
        }

        private void bbNew_Click(object sender, EventArgs e)
        {
            edDatatype.Text = "";
            edCustomValue.Text = "";
            edCargowiseValue.Text = "";
            cw_ID = Guid.Empty;
            edDatatype.Focus();
        }

        private void edDatatype_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                if (this.ActiveControl != null)
                {
                    this.SelectNextControl(this.ActiveControl, true, true, true, true);
                }
                e.Handled = true;
            }
        }
    }
}
