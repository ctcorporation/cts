﻿using CTS_Satellite.DTO;
using CTS_Satellite.Models;
using Ionic.Zip;
using System;
using System.Data;

using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

using static CTS_Satellite.SatelliteErrors;

namespace CTS_Satellite
{
    class NodeResources
    {

        public static Customer GetCustomer(string custCode)
        {
            using (IUnitOfWork uow = new UnitOfWork(new SatelliteContext(Globals.connString())))
            {
                var cust = (uow.Customers.Find(x => x.C_CODE == custCode)).FirstOrDefault();
                return cust;
            }
        }

        public static DataTable ConvertCSVtoDataTable(string strFilePath)
        {
            try
            {
                using (StreamReader sr = new StreamReader(strFilePath))
                {
                    string[] headers = sr.ReadLine().Split(',');
                    DataTable dt = new DataTable();
                    foreach (string header in headers)
                    {
                        dt.Columns.Add(header.Replace("\"", string.Empty));
                    }
                    while (!sr.EndOfStream)
                    {
                        var line = sr.ReadLine().Replace("\"", "");
                        string[] rows = Regex.Split(line, ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                        if (headers.Length == rows.Length)
                        {
                            DataRow dr = dt.NewRow();
                            for (int i = 0; i < headers.Length; i++)
                            {
                                dr[i] = rows[i];
                            }
                            bool emptyCol = true;
                            for (int i = 0; i < dr.ItemArray.Length; i++)
                            {
                                if (!string.IsNullOrEmpty(dr[i].ToString()))
                                {
                                    emptyCol = false;
                                }
                            }
                            if (!emptyCol)
                            {
                                dt.Rows.Add(dr);
                            }

                        }
                    }
                    sr.Close();
                    return dt;
                }
            }
            catch (IOException ex)
            {
                return null;
            }

        }

        public static void AddLabel(Label lb, string value)
        {
            if (lb.InvokeRequired)
            {
                lb.BeginInvoke(new System.Action(delegate { AddLabel(lb, value); }));
                return;
            }
            lb.Text = value;
        }


        public static void AddText(RichTextBox rtb, string value)
        {
            if (rtb.InvokeRequired)
            {
                rtb.BeginInvoke(new System.Action(delegate { AddText(rtb, value); }));
                return;
            }

            rtb.AppendText((value) + Environment.NewLine);
            rtb.ScrollToCaret();

        }
        public static void AddText(RichTextBox rtb, string value, Color color)
        {
            if (rtb.InvokeRequired)
            {
                rtb.BeginInvoke(new System.Action(delegate { AddText(rtb, value, color); }));
                return;
            }
            string[] str = value.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            rtb.DeselectAll();
            rtb.SelectionColor = color;
            rtb.AppendText(value + Environment.NewLine);
            rtb.SelectionFont = new Font(rtb.SelectionFont, FontStyle.Regular);
            rtb.SelectionColor = Color.Black;
            rtb.ScrollToCaret();

        }

        public static void AddProcessingError(ProcessingErrors procError)
        {
            using (IUnitOfWork uow = new UnitOfWork(new SatelliteContext(Globals.connString())))
            {
                Processing_Errors error = new Processing_Errors
                {
                    E_SENDERID = procError.SenderId,
                    E_RECIPIENTID = procError.RecipientId,
                    E_FILENAME = Path.GetFileName(procError.FileName),
                    E_ERRORCODE = procError.ErrorCode.Code.ToString(),
                    E_ERRORDESC = procError.ErrorCode.Description,
                    E_PROCDATE = DateTime.Now,
                    E_P = procError.ProcId
                };
                uow.ProcErrors.Add(error);
                uow.Complete();
            };


        }

        public static void AddTransaction(Guid custid, Guid profile, string fileName, string direction, bool original, TransReference trRefs, string archive)
        {
            using (IUnitOfWork uow = new UnitOfWork(new SatelliteContext(Globals.connString())))
            {
                Transaction tran = new Transaction
                {
                    T_C = custid,
                    T_P = profile,
                    T_FILENAME = Path.GetFileName(fileName),
                    T_DATETIME = DateTime.Now,
                    T_TRIAL = "N",
                    T_DIRECTION = direction,
                    T_REF1 = trRefs.Reference1,
                    T_REF2 = trRefs.Reference2,
                    T_REF3 = trRefs.Reference3,
                    T_REF1TYPE = trRefs.Ref1Type.ToString(),
                    T_REF2TYPE = trRefs.Ref2Type.ToString(),
                    T_REF3TYPE = trRefs.Ref3Type.ToString(),
                    T_ARCHIVE = Path.GetFileName(archive)
                };
                uow.Transactions.Add(tran);
                uow.Complete();
            }

        }

        public static string ExtractFile(string archive, string fileName, string Location)
        {
            string result = string.Empty;
            try
            {
                using (ZipFile zip = ZipFile.Read(archive))
                {
                    if (fileName == "*.*")
                    {
                        foreach (ZipEntry ze in zip)
                        {
                            ze.Extract(Location);
                            result += ze.FileName + ";";

                        }
                    }
                    else
                    {
                        var arcFound = zip.Any(entry => entry.FileName.EndsWith(Path.GetFileName(fileName)));
                        zip.Dispose();
                        if (arcFound)
                        {
                            foreach (ZipEntry e in zip.Where(x => x.FileName == fileName))
                            {
                                e.Extract(Location);
                                result += e.FileName + ";";

                            }

                        }
                        else
                        {
                            result = "Warning: Specified File not found in Archive.";
                        }
                        zip.Dispose();
                    }
                }
                //                var zipFile = ZipFile.Read(@archive);



            }
            catch (Exception ex)
            {
                string strEx = ex.GetType().Name;
                result = "Warning: " + strEx + "Exception found. Error is: " + ex.Message;
            }
            return result;

        }

        public static string GetEnum(string enumType, string mapValue, string extraValue)
        {

            string result = string.Empty;
            using (IUnitOfWork uow = new UnitOfWork(new SatelliteContext(Globals.connString())))
            {
                var enums = (uow.Enums.Find(x => x.CW_ENUMTYPE == enumType && x.CW_MAPVALUE == mapValue)).FirstOrDefault();
                if (enums != null)
                {
                    return enums.CW_ENUM.Trim();
                }
                return null;
            }
        }

        public static string MoveFile(string fileToMove, string folder)
        {
            int icount = 1;

            string filename = Path.GetFileNameWithoutExtension(fileToMove);
            string ext = Path.GetExtension(fileToMove);
            string newfile = Path.Combine(folder, Path.GetFileName(fileToMove));
            while (File.Exists(newfile))
            {
                newfile = Path.Combine(folder, filename + icount + ext);
                icount++;

            }

            try
            {
                File.Move(fileToMove, Path.Combine(folder, newfile));
            }
            catch (Exception ex)
            {
                string strEx = ex.GetType().Name;
                newfile = "Warning: " + strEx + " exception found while moving " + filename;
            }
            return newfile;
        }

        //public static string FtpSend(string file, CustProfileRecord cust)
        //{
        //    string Result = string.Empty;
        //    try
        //    {
        //        SessionOptions sessionOptions = new SessionOptions
        //        {
        //            HostName = cust.P_server.Trim(),
        //            PortNumber = int.Parse(cust.P_port),
        //            UserName = cust.P_username.Trim(),
        //            Password = cust.P_password.Trim()
        //        };
        //        if (cust.P_ssl)
        //        {
        //            sessionOptions.Protocol = Protocol.Sftp;
        //        }
        //        else
        //        {
        //            sessionOptions.Protocol = Protocol.Ftp;
        //        }
        //        //try
        //        //{
        //        TransferOperationResult tr;
        //        using (Session session = new Session())
        //        {
        //            session.Open(sessionOptions);
        //            if (session.Opened)
        //            {
        //                //NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Connected to " + custProfile[0].P_server + ". Sending file  :" + xmlfile + Environment.NewLine + " TO " + sessionOptions.HostName + "/" + custProfile[0].P_path + "/" + Path.GetFileName(xmlfile));
        //                TransferOptions tOptions = new TransferOptions();
        //                tOptions.OverwriteMode = OverwriteMode.Overwrite;
        //                if (string.IsNullOrEmpty(cust.P_path))
        //                {
        //                    cust.P_path = "";
        //                }
        //                else
        //                {
        //                    cust.P_path = cust.P_path + "/";
        //                }
        //                tr = session.PutFiles(file, "/" + cust.P_path + Path.GetFileName(file), true, tOptions);
        //                //NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Results: ");
        //                if (tr.IsSuccess)
        //                {
        //                    Result = "File Sent Ok";
        //                    try
        //                    {
        //                        File.Delete(file);
        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        Result = "File Sent Ok. Delete Error: " + ex.Message;
        //                    }

        //                }
        //                else
        //                {
        //                    Result = "Failed: Transfer Result is invalid: ";
        //                }

        //            }
        //            else
        //            {

        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        Result = "Failed: " + ex.GetType().Name + " " + ex.Message;
        //    }

        //    return Result;
        //}

    }


}