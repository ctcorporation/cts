﻿using CTS_Satellite.DTO;
using CTS_Satellite.Models;
using CTS_Satellite.Resources;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace CTS_Satellite
{
    public partial class frmOrganisation : Form
    {
        private BindingSource bsMap = new BindingSource();
        private Guid _gid = Guid.Empty;

        public frmOrganisation()
        {
            InitializeComponent();
            grdOrgs.AutoGenerateColumns = false;
        }

        private void bbClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bbAdd_Click(object sender, EventArgs e)
        {
            using (IUnitOfWork uow = new UnitOfWork(new SatelliteContext(Globals.ConnString.ConnString)))
            {
                if (_gid == Guid.Empty)
                {
                    Organisation newOrg = new Organisation
                    {
                        OR_CODE = edCargowiseCode.Text.Trim(),
                        OR_Name = edName.Text.Trim(),
                        OR_MapValue = edCode.Text.Trim()
                    };
                    var org = uow.Organisations.Find(x => x.OR_CODE == newOrg.OR_CODE
                                                && x.OR_Name == newOrg.OR_Name
                                                && x.OR_MapValue == newOrg.OR_MapValue).FirstOrDefault();
                }
                else
                {
                    var newOrg = uow.Organisations.Get(_gid);
                    newOrg.OR_CODE = edCargowiseCode.Text.Trim();
                    newOrg.OR_Name = edName.Text.Trim();

                }

                if (uow.Complete() > 0)
                {
                    MessageBox.Show("Mapping Added", "Add/Update Mapping", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    FillOrgs();
                    bbAdd.Text = "&Add";
                    _gid = Guid.Empty;
                }
            }

        }

        private void frmOrganisation_Load(object sender, EventArgs e)
        {
            FillOrgs();
        }

        private void FillOrgs()
        {
            using (IUnitOfWork uow = new UnitOfWork(new SatelliteContext(Globals.ConnString.ConnString)))
            {
                var orgsList = uow.Organisations.GetAll().OrderBy(x=>x.OR_CODE).ToList();
                bsMap.DataSource = new SortableBindingList<Organisation>(orgsList);
                grdOrgs.DataSource = bsMap;

            }


        }

        private void grdOrgs_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                GetMapData((Guid)grdOrgs.Rows[e.RowIndex].Cells["OR_ID"].Value);
                bbAdd.Text = "&Update";
            }
        }

        private void GetMapData(Guid value)
        {
            using (IUnitOfWork uow = new UnitOfWork(new SatelliteContext(Globals.ConnString.ConnString)))
            {
                var val = uow.Organisations.Get(value);
                if (val == null)
                {
                    return;
                }
                _gid = value;
                edCode.Text = val.OR_MapValue.Trim();
                edName.Text = val.OR_Name.Trim();
                edCargowiseCode.Text = val.OR_CODE.Trim();


            }
        }
    }

}
