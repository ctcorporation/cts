﻿namespace CTS_Satellite.Models
{
    public class DataItem
    {
        public string ItemValue { get; set; }

        public string Text { get; set; }
    }
}
