﻿using CTS_Satellite.Models;

namespace CTS_Satellite.Repository
{
    public class TransactionRepository : GenericRepository<Transaction>, ITransactionRepository
    {
        public TransactionRepository(SatelliteContext context)
            : base(context)
        {

        }

        public SatelliteContext SatelliteContext
        {
            get
            {
                return Context as SatelliteContext;
            }
        }
    }
}
