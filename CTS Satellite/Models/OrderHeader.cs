﻿using System;

namespace CTS_Satellite.Models
{
    public class OrderHeader
    {
        public string PONumber { get; set; }

        public string IncoTerms { get; set; }

        public string Brandcode { get; set; }
        public string VendorCode { get; set; }

        public string ShipVia { get; set; }

        public string PortOfOrigin { get; set; }

        public string DestPort { get; set; }

        public DateTime? DcDeliveryDate { get; set; }

        public string ConsolidationRemarks { get; set; }

        public DateTime? WHDate { get; set; }

        public string Vendor { get; set; }

        public DateTime? HandOver { get; set; }

        public DateTime? StoreDate { get; set; }

        public string FabricContent { get; set; }

        public string GarmentType { get; set; }

    }
}
