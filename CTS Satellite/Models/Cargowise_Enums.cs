namespace CTS_Satellite.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Cargowise_Enums
    {
        [Key]
        public Guid CW_ID { get; set; }

        [StringLength(50)]
        public string CW_ENUMTYPE { get; set; }

        [StringLength(100)]
        public string CW_ENUM { get; set; }

        [StringLength(100)]
        public string CW_MAPVALUE { get; set; }

        [StringLength(20)]
        public string CW_EXTRAVALUE { get; set; }

        public ICollection<CustomMapping> MappingList { get; set; }
    }
}
