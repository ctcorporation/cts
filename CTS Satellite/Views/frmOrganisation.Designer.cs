﻿namespace CTS_Satellite
{
    partial class frmOrganisation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bbClose = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.edCode = new System.Windows.Forms.TextBox();
            this.edName = new System.Windows.Forms.TextBox();
            this.edCargowiseCode = new System.Windows.Forms.TextBox();
            this.bbAdd = new System.Windows.Forms.Button();
            this.grdOrgs = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.grdOrgs)).BeginInit();
            this.SuspendLayout();
            // 
            // bbClose
            // 
            this.bbClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bbClose.Location = new System.Drawing.Point(589, 451);
            this.bbClose.Name = "bbClose";
            this.bbClose.Size = new System.Drawing.Size(75, 23);
            this.bbClose.TabIndex = 0;
            this.bbClose.Text = "&Close";
            this.bbClose.UseVisualStyleBackColor = true;
            this.bbClose.Click += new System.EventHandler(this.bbClose_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 410);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Customer Code";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(154, 410);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Customer Name";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(463, 411);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Cargowise Code";
            // 
            // edCode
            // 
            this.edCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.edCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edCode.Location = new System.Drawing.Point(32, 427);
            this.edCode.Name = "edCode";
            this.edCode.Size = new System.Drawing.Size(100, 20);
            this.edCode.TabIndex = 5;
            // 
            // edName
            // 
            this.edName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.edName.Location = new System.Drawing.Point(157, 427);
            this.edName.Name = "edName";
            this.edName.Size = new System.Drawing.Size(283, 20);
            this.edName.TabIndex = 6;
            // 
            // edCargowiseCode
            // 
            this.edCargowiseCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.edCargowiseCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edCargowiseCode.Location = new System.Drawing.Point(466, 427);
            this.edCargowiseCode.Name = "edCargowiseCode";
            this.edCargowiseCode.Size = new System.Drawing.Size(100, 20);
            this.edCargowiseCode.TabIndex = 7;
            // 
            // bbAdd
            // 
            this.bbAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bbAdd.Location = new System.Drawing.Point(588, 424);
            this.bbAdd.Name = "bbAdd";
            this.bbAdd.Size = new System.Drawing.Size(75, 23);
            this.bbAdd.TabIndex = 8;
            this.bbAdd.Text = "&Add";
            this.bbAdd.UseVisualStyleBackColor = true;
            this.bbAdd.Click += new System.EventHandler(this.bbAdd_Click);
            // 
            // grdOrgs
            // 
            this.grdOrgs.AllowUserToAddRows = false;
            this.grdOrgs.AllowUserToDeleteRows = false;
            this.grdOrgs.AllowUserToOrderColumns = true;
            this.grdOrgs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdOrgs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdOrgs.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4});
            this.grdOrgs.Location = new System.Drawing.Point(13, 13);
            this.grdOrgs.Name = "grdOrgs";
            this.grdOrgs.ReadOnly = true;
            this.grdOrgs.Size = new System.Drawing.Size(651, 341);
            this.grdOrgs.TabIndex = 9;
            this.grdOrgs.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdOrgs_CellContentDoubleClick);
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "OR_CODE";
            this.Column1.HeaderText = "Org. Code";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 200;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column2.DataPropertyName = "OR_NAME";
            this.Column2.HeaderText = "Organisation Name";
            this.Column2.MinimumWidth = 200;
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 200;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "OR_MAPVALUE";
            this.Column3.HeaderText = "Map to Code";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 150;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "OR_ID";
            this.Column4.HeaderText = "Org ID";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Visible = false;
            // 
            // frmOrganisation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(676, 486);
            this.Controls.Add(this.grdOrgs);
            this.Controls.Add(this.bbAdd);
            this.Controls.Add(this.edCargowiseCode);
            this.Controls.Add(this.edName);
            this.Controls.Add(this.edCode);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bbClose);
            this.Name = "frmOrganisation";
            this.Text = "Organisation Mapping";
            this.Load += new System.EventHandler(this.frmOrganisation_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdOrgs)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bbClose;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox edCode;
        private System.Windows.Forms.TextBox edName;
        private System.Windows.Forms.TextBox edCargowiseCode;
        private System.Windows.Forms.Button bbAdd;
        private System.Windows.Forms.DataGridView grdOrgs;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
    }
}