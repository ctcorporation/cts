﻿using CTS_Satellite.Models;

namespace CTS_Satellite.Repository
{
    public interface IProcErrorsRepository : IGenericRepository<Processing_Errors>
    {
        SatelliteContext SatelliteContext { get; }
    }
}