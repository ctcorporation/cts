﻿using CTS_Satellite.Models;

namespace CTS_Satellite.Repository
{
    public interface IEnumRepository : IGenericRepository<Cargowise_Enums>
    {
        SatelliteContext SatelliteContext { get; }
    }
}