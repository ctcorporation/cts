﻿using CTS_Satellite.DTO;
using CTS_Satellite.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Serialization;
using XMLLocker.Cargowise.XUS;

namespace CTS_Satellite
{
    public class CityChicProcesses
    {

        #region Members
        private DataTable _orderTable;
        private List<OrderHeader> orders;
        private List<OrderLines> orderLines;


        #endregion

        #region Properties
        public DataTable OrderTable
        {
            get { return _orderTable; }
            set { _orderTable = value; }
        }

        public string ErrMsg { get; set; }
        public string CustCode { get; set; }
        public string OutDir { get; set; }


        #endregion

        #region Constructors
        public CityChicProcesses(DataTable dt, string custcode, string outDir)
        {
            OutDir = outDir;
            CustCode = custcode;
            _orderTable = dt;
        }
        #endregion

        #region Methods
        public bool ProcessFiles()
        {
            CreateOrderLists(_orderTable);
            return CreateCWPO_UXML();

        }
        public bool CreateCWPO_UXML()
        {

            ShipmentOrder trackingOrder = new ShipmentOrder();
            UniversalInterchangeHeader header = new UniversalInterchangeHeader();
            List<UniversalShipmentData> orderColl = new List<UniversalShipmentData>();
            UniversalShipmentData shipmentData = new UniversalShipmentData();
            List<ShipmentOrderOrderLineCollectionOrderLine> orderLineColl = new List<ShipmentOrderOrderLineCollectionOrderLine>();
            Shipment shipment = new Shipment();
            DataContext dc = new DataContext();
            List<DataTarget> dtColl = new List<DataTarget>();
            DataTarget dt = new DataTarget();
            List<OrganizationAddress> orgAddressColl = new List<OrganizationAddress>();
            OrganizationAddress orgAdd = new OrganizationAddress();
            string alertMsg = "";
            foreach (var order in orders)
            {

                header = new UniversalInterchangeHeader();
                orgAddressColl = new List<OrganizationAddress>();
                shipment = new Shipment();
                dtColl = new List<DataTarget>();
                dtColl.Add(new DataTarget
                {
                    Type = "OrderManagerOrder"
                });
                CodeDescriptionPair eventType = new CodeDescriptionPair
                {
                    Code = "OPL"
                };
                dc = new DataContext();
                dc.DataProvider = "CITYCHIC";
                dc.DataTargetCollection = dtColl.ToArray();
                dc.ActionPurpose = new CodeDescriptionPair
                {
                    Code = "CTC",
                    Description = "File Imported by CTC Node"
                };
                dc.EventType = new CodeDescriptionPair
                {
                    Code = "DIM",
                    Description = "Import Data"
                };
                shipment.DataContext = dc;
                shipment.ShipmentIncoTerm = new IncoTerm { Code = order.IncoTerms };
                // 15/05/2020 - No longer need to filter Inco Terms. All are now accepted. 
                //switch (order.IncoTerms)
                //{
                //    case "FOB":
                //        shipment.ShipmentIncoTerm = new IncoTerm { Code = order.IncoTerms };
                //        break;

                //    case "FIS":
                //        ErrMsg += "Incoterms FIS invalid. Order " + order.PONumber + " Skipped." + Environment.NewLine;
                //        continue;
                //        break;
                //    case "DDP":
                //        ErrMsg += "Incoterms DDP invalid. Order " + order.PONumber + " Skipped." + Environment.NewLine;
                //        continue;
                //        break;
                //    case "DCA":
                //        ErrMsg += "Incoterms DCA invalid. Order " + order.PONumber + " Skipped." + Environment.NewLine;
                //        continue;
                //        break;
                //    case "FCA":
                //        shipment.ShipmentIncoTerm = new IncoTerm { Code = order.IncoTerms };
                //        //ErrMsg += "Incoterms FCA invalid. Order " + order.PONumber + "Skipped." + Environment.NewLine;
                //        continue;
                //        break;
                //    case "DAP":
                //        ErrMsg += "Incoterms DAP invalid. Order " + order.PONumber + " Skipped." + Environment.NewLine;
                //        continue;
                //        break;

                //}
                if (shipment.LocalProcessing == null)
                {
                    shipment.LocalProcessing = new ShipmentLocalProcessing();
                }
                List<Date> dateColl = new List<Date>();
                Date date = new Date();
                date.Type = DateType.ExWorksRequiredBy;
                if (order.HandOver != null)
                {
                    if (order.HandOver <= DateTime.Today.AddDays(Globals.glExFactDays))
                    {
                        ErrMsg += "Ex-Works date too close or Hand Over Date has passed to update. Order " + order.PONumber + " skipped." + Environment.NewLine;
                        continue;
                    }

                    date.Value = order.HandOver?.ToString("s");
                    date.IsEstimate = false;
                    date.IsEstimateSpecified = true;
                }
                dateColl.Add(date);
                if (order.DcDeliveryDate != null)
                {
                    dateColl.Add(new Date
                    {
                        Type = DateType.DeliveryRequiredBy,
                        Value = order.DcDeliveryDate?.ToString("s"),
                        IsEstimate = true,
                        IsEstimateSpecified = true
                    });

                    //List<ShipmentMilestone> mileStoneColl = new List<ShipmentMilestone>();
                    //mileStoneColl.Add(new ShipmentMilestone
                    //{
                    //    EstimatedDate = order.DcDeliveryDate?.ToString("s"),
                    //    EventCode = "ARV",
                    //    ConditionType = "RFP",
                    //    ConditionReference = "LOC=&lt;LastLeg.Destination&gt;"
                    //});
                    //shipment.MilestoneCollection = mileStoneColl.ToArray();
                    if (order.DcDeliveryDate != null)
                    {

                        shipment.LocalProcessing.EstimatedDelivery = order.DcDeliveryDate?.ToString("s");
                    }
                }

                if (order.StoreDate != null)
                {
                    shipment.LocalProcessing.DeliveryRequiredBy = order.StoreDate?.ToString("s");
                }
                shipment.DateCollection = dateColl.ToArray();

                orgAddressColl.Add(new OrganizationAddress
                {
                    AddressType = "ConsigneeDocumentaryAddress",
                    OrganizationCode = !string.IsNullOrEmpty(order.Brandcode) ? order.Brandcode : "CITY CHIC"
                });
                orgAddressColl.Add(new OrganizationAddress
                {
                    AddressType = "ConsignorDocumentaryAddress",
                    OrganizationCode = order.VendorCode,
                    CompanyName = order.Vendor
                });
                shipment.OrganizationAddressCollection = orgAddressColl.ToArray();
                var portOfDest = NodeResources.GetEnum("UNLOCODE", order.DestPort, string.Empty);
                shipment.PortOfDestination = new UNLOCO
                {
                    Code = !string.IsNullOrEmpty(portOfDest) ? portOfDest : order.DestPort
                };
                string country = NodeResources.GetEnum("UNLOCODE", order.PortOfOrigin, order.ShipVia);
                if (!string.IsNullOrEmpty(country))
                {
                    shipment.CountryOfSupply = new Country { Code = country.Substring(0, 2) };
                }
                country = NodeResources.GetEnum("UNLOCODE", order.PortOfOrigin, order.PortOfOrigin);
                if (!string.IsNullOrEmpty(country))
                {
                    {
                        shipment.TransportMode = new CodeDescriptionPair { Code = order.ShipVia };
                        shipment.PortOfLoading = new UNLOCO { Code = country };
                    }
                }
                trackingOrder.OrderNumber = order.PONumber;
                //Build Order Lines from orderLines array.
                orderLineColl = new List<ShipmentOrderOrderLineCollectionOrderLine>();
                // Map and Add Custom Fields to PO Header 
                List<CustomizedField> custHeaderFieldColl = new List<CustomizedField>();
                if (!string.IsNullOrEmpty(order.ConsolidationRemarks))
                {
                    custHeaderFieldColl.Add(new CustomizedField
                    {
                        DataType = CustomizedFieldDataType.String,
                        Key = "Consolidation Remarks",
                        Value = order.ConsolidationRemarks
                    });
                }
                if (order.DcDeliveryDate != null)
                {
                    custHeaderFieldColl.Add(new CustomizedField
                    {
                        DataType = CustomizedFieldDataType.DateTime,
                        Key = "DC Delivery Date",
                        Value = order.DcDeliveryDate?.ToString("s")
                    });
                }
                if (!string.IsNullOrEmpty(order.GarmentType))
                {
                    custHeaderFieldColl.Add(new CustomizedField
                    {
                        DataType = CustomizedFieldDataType.String,
                        Key = "Garment Type",
                        Value = order.GarmentType

                    });
                }
                if (!string.IsNullOrEmpty(order.FabricContent))
                {
                    custHeaderFieldColl.Add(new CustomizedField
                    {
                        DataType = CustomizedFieldDataType.String,
                        Key = "Fabric Content",
                        Value = order.FabricContent
                    });
                }
                if (!string.IsNullOrEmpty(order.Brandcode))
                {
                    custHeaderFieldColl.Add(new CustomizedField
                    {
                        DataType = CustomizedFieldDataType.String,
                        Key = "Brand",
                        Value = order.Brandcode
                    });
                }
                shipment.CustomizedFieldCollection = custHeaderFieldColl.ToArray();
                trackingOrder.OrderLineCollection = CreateOrderLines(order.PONumber);
                shipment.Order = trackingOrder;
                shipmentData.Shipment = shipment;

                UniversalInterchange interchange = new UniversalInterchange
                {
                    Header = new UniversalInterchangeHeader
                    {
                        RecipientID = CustCode,
                        SenderID = "CITCHISYD1"
                    },
                    Body = new UniversalInterchangeBody
                    {
                        BodyField = new UniversalShipmentData
                        {
                            Shipment = shipment,
                            version = "1.1"
                        }
                    },
                    version = "1.1"
                };

                XmlFileBuilder xmlBuilder = new XmlFileBuilder(CustCode, OutDir);
                var cwNSUniversal = new XmlSerializerNamespaces();
                cwNSUniversal.Add("", "http://www.cargowise.com/Schemas/Universal/2011/11");
                if (xmlBuilder.CreateFile(order.PONumber, interchange, cwNSUniversal))
                {
                    ErrMsg += "Order File for " + order.PONumber + "created ok" + Environment.NewLine;
                }
                else
                {
                    ErrMsg += xmlBuilder.ErrMsg + Environment.NewLine;

                }
            }
            return true;

        }

        private ShipmentOrderOrderLineCollection CreateOrderLines(string pONumber)
        {
            ShipmentOrderOrderLineCollection oColl = new ShipmentOrderOrderLineCollection
            {
                Content = CollectionContent.Complete,
                ContentSpecified = true
            };
            List<ShipmentOrderOrderLineCollectionOrderLine> trackingOrdlines = new List<ShipmentOrderOrderLineCollectionOrderLine>();
            int lineNo = 0;

            var lines = (from x in orderLines
                         where x.PONumber == pONumber
                         select x).ToList();
            foreach (var line in lines)
            {
                lineNo++;
                ShipmentOrderOrderLineCollectionOrderLine orderLine = new ShipmentOrderOrderLineCollectionOrderLine();
                orderLine.LineNumber = line.LineNumber;
                orderLine.LineNumberSpecified = true;
                orderLine.Product = new Product { Code = line.ProdCode, Description = line.Descr };
                orderLine.OrderedQty = line.Qty;
                orderLine.OrderedQtySpecified = true;
                orderLine.OrderedQtyUnit = new CodeDescriptionPair { Code = "UNT" };
                List<CustomizedField> custFieldColl = new List<CustomizedField>();
                CustomizedField custFld = new CustomizedField();
                if (!string.IsNullOrEmpty(line.PackCode))
                {
                    custFieldColl.Add(new CustomizedField
                    {
                        DataType = CustomizedFieldDataType.String,
                        Key = "Pack Code",
                        Value = line.PackCode
                    });
                }
                if (!string.IsNullOrEmpty(line.Color))
                {
                    custFieldColl.Add(new CustomizedField
                    {
                        DataType = CustomizedFieldDataType.String,
                        Key = "Color",
                        Value = line.Color
                    });
                }
                if (!string.IsNullOrEmpty(line.HSCode))
                {
                    custFieldColl.Add(new CustomizedField
                    {
                        DataType = CustomizedFieldDataType.String,
                        Key = "HS Code",
                        Value = line.HSCode
                    });
                }
                if (!string.IsNullOrEmpty(line.Channel))
                {
                    custFieldColl.Add(new CustomizedField
                    {
                        DataType = CustomizedFieldDataType.String,
                        Key = "Distribution Channel",
                        Value = line.Channel
                    });
                }
                if (!string.IsNullOrEmpty(line.SizeHeader))
                {
                    custFieldColl.Add(new CustomizedField
                    {
                        DataType = CustomizedFieldDataType.String,
                        Key = "Size Header",
                        Value = line.SizeHeader
                    });
                }
                if (!string.IsNullOrEmpty(line.SizeRatio))
                {
                    custFieldColl.Add(new CustomizedField
                    {
                        DataType = CustomizedFieldDataType.String,
                        Key = "Pack Ratio",
                        Value = line.SizeRatio
                    });
                }
                orderLine.CustomizedFieldCollection = custFieldColl.ToArray();
                trackingOrdlines.Add(orderLine);
            }
            oColl.OrderLine = trackingOrdlines.ToArray();
            return oColl;

        }
        #endregion

        #region Helpers

        public void CreateOrderLists(DataTable dt)
        {
            orders = new List<OrderHeader>();
            orderLines = new List<OrderLines>();
            foreach (DataRow dr in dt.Rows)
            {
                // int PoNumber = 0;
                string searchExp = "[PO Number (7)]= '" + dr["PO Number (7)"].ToString() + "'";
                var orderHeader = FindOrder(dr["PO Number (7)"].ToString(), orders);
                if (orderHeader == null)
                {
                    orderHeader = new OrderHeader
                    {
                        PONumber = (string)dr["PO Number (7)"],
                        Brandcode = (string)dr["Brand (3)"],
                        VendorCode = (string)dr["Vendor Code (5)"],
                        Vendor = (string)dr["Vendor (4)"],
                        IncoTerms = (string)dr["INCOTERMS"],
                        DestPort = (string)dr["DESTINATION PORT"],
                        PortOfOrigin = (string)dr["PORT OF ORIGIN"],
                        FabricContent = (string)dr["FIBER CONTENT"],
                        GarmentType = (string)dr["Maj Cat (12)"],
                        ConsolidationRemarks = (string)dr["CONSOLIDATION REMARKS"],
                        ShipVia = (string)dr["SHIP VIA"]
                    };
                    try
                    {
                        var handOverDate = (DateTime)GetDateFromString((string)dr["HAND OVER"]);
                        if (handOverDate != null)
                        {
                            orderHeader.HandOver = handOverDate;
                        }
                        var storeDate = (DateTime)GetDateFromString((string)dr["INTO STORE DATE"]);
                        if (storeDate != null)
                        {
                            orderHeader.StoreDate = storeDate;
                        }
                        var dcDeliverDate = (DateTime)GetDateFromString((string)dr["DC DELIVERY DATE"]);
                        if (dcDeliverDate != null)
                        {
                            orderHeader.DcDeliveryDate = dcDeliverDate;
                        }
                    }
                    catch (Exception ex)
                    {
                        string strEx = ex.GetType().Name;
                    }

                    orders.Add(orderHeader);
                }
                var newLineNo = GetNextLineNo(orderHeader.PONumber, orderLines);
                var linesToBeAdded = CreateOrderLine(dr, newLineNo, orderHeader.PONumber);
                orderLines.Add(linesToBeAdded);
            }
        }

        private OrderLines CreateOrderLine(DataRow dr, int newLineNo, string poNumber)
        {
            //List<OrderLines> ordLines = new List<OrderLines>();
            //if ((string)dr["DISTR. CHAN."] == "RETAIL")
            //{
            int qty = 0;
            OrderLines line = new OrderLines
            {
                LineNumber = newLineNo,
                PONumber = poNumber,
                Channel = (string)dr["DISTR. CHAN."],
                Color = (string)dr["Colour (15)"],
                Descr = (string)dr["Description (14)"],
                PackCode = (string)dr["RETAIL PACK CODE"],
                HSCode = (string)dr["HS CODE"],
                PackSize = (string)dr["Pack Size (17)"],
                ProdCode = (string)dr["STYLE NUMBER"],
                Qty = int.TryParse(dr["Total Units"].ToString(), out qty) ? qty : 0,
                SizeHeader = (string)dr["SIZE HEADER"],
                SizeRatio = (string)dr["Size Ratio (43)"]
            };
            // orderLines.Add(line);
            return line;
        }

        private int GetNextLineNo(string pONumber, List<OrderLines> orderLines)
        {
            var _lineNo = 0;
            if (orderLines.Count == 0)
            {
                return 1;
            }
            var lineNo = (from x in orderLines
                          where x.PONumber == pONumber
                          select x).OrderByDescending(l => l.LineNumber).FirstOrDefault();
            if (lineNo == null)
            {
                return 1;
            }
            _lineNo = lineNo.LineNumber + 1;

            return _lineNo;

        }

        private DateTime? GetDateFromString(string dateString)
        {
            DateTime dt = new DateTime();
            if (DateTime.TryParse(dateString, out dt))
            {
                return dt;
            }
            return null;
        }

        private OrderHeader FindOrder(string orderNo, List<OrderHeader> orderHeader)
        {
            var foundOrder = (from x in orderHeader
                              where x.PONumber == orderNo
                              select x).FirstOrDefault();
            return foundOrder;
        }
        public Globals.Customer GetOrgCode(string custCode)
        {
            Globals.Customer result = new Globals.Customer();
            using (DTO.IUnitOfWork uow = new UnitOfWork(new SatelliteContext(Globals.ConnString.ConnString)))
            {
                var org = uow.Organisations.Find(x => x.OR_MapValue == custCode).FirstOrDefault();
                if (org != null)
                {
                    result.Code = org.OR_CODE;
                    result.ShortName = org.OR_ShortCode;
                    result.CustomerName = org.OR_Name;
                    return result;
                }

            }
            return null;
        }
        #endregion

    }
}
