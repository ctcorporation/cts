﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.SqlServer;

namespace CTS_Satellite.Models
{
    public class NodeDBConfiguration : DbConfiguration
    {

        public NodeDBConfiguration()
        {
            SetTransactionHandler(SqlProviderServices.ProviderInvariantName, () => new CommitFailureHandler());
            SetExecutionStrategy(SqlProviderServices.ProviderInvariantName, () => new SqlAzureExecutionStrategy());
        }


    }

    [DbConfigurationType(typeof(NodeDBConfiguration))]
    public partial class NodeDataContext : DbContext
    {

        public NodeDataContext(string connString)
            : base(connString)
        {

        }
        public NodeDataContext()
            : base("name=NodedataEntities")
        {
        }


        public virtual DbSet<HeartBeats> HeartBeats { get; set; }
    }

}
