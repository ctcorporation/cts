﻿using CTS_Satellite.Repository;
using System;

namespace CTS_Satellite.DTO
{
    public interface IUnitOfWork : IDisposable
    {
        ICargowiseContextRepository CWContext { get; }
        IDTSRepository DTServices { get; }

        IHeartBeatRepository HBeats { get; }
        INodeCustRepository NodeCust { get; }
        ITransactionRepository Transactions { get; }
        IProcErrorsRepository ProcErrors { get; }
        ICustomerProfileRepository Profiles { get; }
        ICustomerRepository Customers { get; }
        ICustomMappingRepository CustomMaps { get; }
        IEnumRepository Enums { get; }
        IOrganisationRepository Organisations { get; }
        IFileDescriptionRepository FileDescriptions { get; }
        int Complete();
        int NodeComplete();
    }
}