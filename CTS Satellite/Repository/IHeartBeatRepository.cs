﻿using CTS_Satellite.Models;

namespace CTS_Satellite.Repository
{
    public interface IHeartBeatRepository : IGenericNodeRepository<HeartBeats>
    {
        NodeDataContext NodeDataContext { get; }
    }
}