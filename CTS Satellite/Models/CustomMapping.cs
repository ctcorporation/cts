namespace CTS_Satellite.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("CustomMapping")]
    public partial class CustomMapping
    {
        [Key]
        public Guid CF_ID { get; set; }

        [StringLength(50)]
        public string CF_NAME { get; set; }

        public Guid? CF_P { get; set; }

        [StringLength(50)]
        public string CF_ColumnFrom { get; set; }

        [StringLength(50)]
        public string CF_ColumnTo { get; set; }

        [StringLength(20)]
        public string CF_TableName { get; set; }

        public Guid? CF_CW { get; set; }

        public virtual Cargowise_Enums Cargowise_Enums { get; set; }
    }
}
