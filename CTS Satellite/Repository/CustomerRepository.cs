﻿using CTS_Satellite.Models;

namespace CTS_Satellite.Repository
{
    public class CustomerRepository : GenericRepository<Models.Customer>, ICustomerRepository
    {
        public CustomerRepository(SatelliteContext context)
            : base(context)
        {

        }

        public SatelliteContext SatelliteContext
        {
            get
            {
                return Context as SatelliteContext;
            }
        }
    }
}
