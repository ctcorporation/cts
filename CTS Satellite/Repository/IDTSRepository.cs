﻿using CTS_Satellite.Models;

namespace CTS_Satellite.Repository
{
    public interface IDTSRepository : IGenericRepository<DTS>
    {
        SatelliteContext SatelliteContext { get; }
    }
}