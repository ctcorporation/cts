﻿namespace CTS_Satellite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class navPropsForCustomMapping : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CustomMapping", "CF_CW", c => c.Guid());
            AddColumn("dbo.CustomMapping", "Cargowise_Enums_CW_ID", c => c.Guid());
            CreateIndex("dbo.CustomMapping", "Cargowise_Enums_CW_ID");
            AddForeignKey("dbo.CustomMapping", "Cargowise_Enums_CW_ID", "dbo.Cargowise_Enums", "CW_ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CustomMapping", "Cargowise_Enums_CW_ID", "dbo.Cargowise_Enums");
            DropIndex("dbo.CustomMapping", new[] { "Cargowise_Enums_CW_ID" });
            DropColumn("dbo.CustomMapping", "Cargowise_Enums_CW_ID");
            DropColumn("dbo.CustomMapping", "CF_CW");
        }
    }
}
