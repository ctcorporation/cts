﻿using CTS_Satellite.Models;
using System.Linq;

namespace CTS_Satellite.Repository
{
    public class CustomMappingRepository : GenericRepository<CustomMapping>, ICustomMappingRepository
    {
        public CustomMappingRepository(SatelliteContext context)
            : base(context)
        {

        }



        public SatelliteContext SatelliteContext
        {
            get
            {
                return Context as SatelliteContext;
            }
        }
    }
}
