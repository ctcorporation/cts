﻿using CTS_Satellite.Models;

namespace CTS_Satellite.Repository
{
    public interface ICustomerProfileRepository : IGenericRepository<Profile>
    {
        SatelliteContext SatelliteContext { get; }
    }
}