﻿using CTS_Satellite.Models;

namespace CTS_Satellite.Repository
{
    public interface ICustomerRepository : IGenericRepository<Models.Customer>
    {
        SatelliteContext SatelliteContext { get; }
    }
}