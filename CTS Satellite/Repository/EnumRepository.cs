﻿using CTS_Satellite.Models;

namespace CTS_Satellite.Repository
{
    public class EnumRepository : GenericRepository<Cargowise_Enums>, IEnumRepository
    {
        public EnumRepository(SatelliteContext context)
            : base(context)
        {

        }
        public SatelliteContext SatelliteContext
        {
            get { return Context as SatelliteContext; }
        }
    }
}
