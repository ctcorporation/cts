﻿using CTS_Satellite.Models;

namespace CTS_Satellite.Repository
{
    public interface INodeCustRepository : IGenericNodeRepository<Customer>
    {
        NodeDataContext NodeDataContext { get; }
    }
}